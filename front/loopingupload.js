import React, {useState,useEffect} from 'react';
import axios from 'axios';
import RadioButtonRN from 'radio-buttons-react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import {upload_img, upload_video} from '../assets/images'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


// Import required components
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  PTRView,
  Image,
  Linking
} from 'react-native';
import {RNS3} from 'react-native-aws3';
import {launchImageLibrary} from 'react-native-image-picker';
import AppContext from '../../AppContext';
import { Authheader } from '../../keys';

const Loopingupload = ({route,navigation}) => {
  const [filePath, setFilePath] = useState({});
  const [fileName, setFileName] = useState('');
  const [vendorId, setVendorId] = useState('');
  const [open1, set_open1] = useState(false);
  const [data_type,setDataType] = useState([{label: 'Image'},{label: 'Video'}]);
  const [type,setType] = useState("image/jpeg");
  const [uploadSuccessMessage, setUploadSuccessMessage] = useState('');
  const {customer_mob,customer_name} = route.params;
  const myContext=React.useContext(AppContext);
  const [vendor_list,set_vendor_list]=useState([{label: 'banana', value: 'banana'}]);
  const fetch_vendor_details = async () => {
       
        await axios.get(`http://35.154.70.53:5000/vendor/fetch?community_id=${myContext.community}`,Authheader)
        .then(res =>{
            let temp = [];
            res.data.forEach(e=>{
              temp.push({label:e.name,value:e.name});
            })
            set_vendor_list([...temp]);

        }).catch(err => {
            if(err.response)
            {   if (err.response.data) {
                console.log(err.response.data.errors);
                err.response.data.errors.map((e) => {
                   
                   console.log("debug e is ", e);
                   return e;
                }); }
                else{
                    console.log("Network error");
                }
            }
            else if (err.request){
                console.log("2 error->", err.message);
            }
            else {
                // Something happened in setting up the request and triggered an Error
                console.log('e Error is ', err.message);
              }
            
        })
    }

    useEffect( () => {
        const unsubscribe = navigation.addListener('focus', () => {
             fetch_vendor_details();
          });
          return unsubscribe;
        }, [navigation]);
    

   /* const _refresh =  ()=> {
        return new Promise((resolve) => {
           fetch_vendor_details()
           .then(r => resolve())
          
        });
      } */


  const chooseFile = () => {
    let options = {
      mediaType: (type == 'image/jpeg') ? 'photo' : 'video',
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      setUploadSuccessMessage('');
      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      setFilePath(response.assets[0]);
    });
  };



  const uploadFile = () => {
    console.log(filePath);
    if (Object.keys(filePath).length == 0) {
      alert('Please select image first');
      return;
    }
    RNS3.put(
      {
        // `uri` can also be a file system path (i.e. file://)
        uri: filePath.uri,
        name: filePath.fileName,
        type: type,
      },
      {
        keyPrefix: 'feedback/', // Ex. myuploads/
        bucket: 'chotuposterimages', // Ex. aboutreact
        region: 'ap-south-1', // Ex. ap-south-1
        accessKey: 'AKIAUA3PQFBUXE6TGIJR',
        // Ex. AKIH73GS7S7C53M46OQ
        secretKey: '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',
        // Ex. Pt/2hdyro977ejd/h2u8n939nh89nfdnf8hd8f8fd
        successActionStatus: 201,
      },
    )
      .progress((progress) =>
        setUploadSuccessMessage(
          `Uploading: ${progress.loaded / progress.total} (${
            progress.percent
          }%)`,
        ),
      )
      .then((response) => {
        if (response.status !== 201)
          alert('Failed to upload image to S3');
        console.log(response.body);
        console.log("Before post");
        const authdetails={
          name:fileName,
          community_id:myContext.community,
          vendor_id:vendorId,
          customer_name:customer_name,
          customer_num:customer_mob
        }
        console.log(authdetails);
        axios.post(`http://35.154.70.53:5000/feedback/add`,authdetails,Authheader);
        console.log("After post");
        Linking.openURL("https://wa.me/91"+customer_mob+"?text=CHOTU received your feedback")
        setFilePath('');
        let {
          bucket,
          etag,
          key,
          location
        } = response.body.postResponse;
        setFileName(key);
        setUploadSuccessMessage(
          `Uploaded Successfully: 
          \n1. bucket => ${bucket}
          \n2. etag => ${etag}
          \n3. key => ${key}
          \n4. location => ${location}`,
        );
        console.log(key);
      });
  };

  return (
    //style={styles.container}


      <View >
        <View style={{backgroundColor:"#0E4C75",flexDirection:'row',height:hp("25%"),width:wp("100%"),justifyContent:"center",textAlign:'center'}}>
            <View style={{justifyContent:"center",textAlign:'center'}}>
                <Image style={{width:wp("44%"),height:hp("18%"),justifyContent:"center",}} source={require('../assets/images/feed_img.png')}/>
                <View style={{bottom:hp("4.5%")}}>
                <Text style ={{fontSize:wp("5%"),fontWeight:'bold',color:'#FFFFFF',textAlign:'center',}}>{customer_name}</Text>
                <Text style ={{fontSize:wp("3%"),color:'#FFFFFF',textAlign:'center',}}>{customer_mob}</Text>
                </View>
            </View>
        </View>
       {/* dropdown */}
           <ScrollView nestedScrollEnabled >
          <View  style={{backgroundColor:"#F2F8FF",height:hp("100%"),}}>
           <View>
          <View style={styles4.inputLogin}>
          
          <DropDownPicker
                    open={open1}
                    value={vendorId}
                    placeholder="Select a Vendor" 
                    
                    
                    listMode="SCROLLVIEW"
                    items={vendor_list}
                    //change BR
                    style={{height:hp("6.2%"),borderRadius: 10,marginBottom:hp("17%")}}
                    searchable={true}
                    setOpen={set_open1}
                    setValue={setVendorId}
                    setItems={set_vendor_list}
                    
          /> 
         
          </View> 
           
        </View>

        {/* Button or touchables or flexbox card */}
        
        <View
         style={{
            flex: 1,
            flexDirection:"row",
            justifyContent: "center",
            bottom:hp("15%")
          }}
        >
           <View>
             <TouchableOpacity>
                <Image source={require("../assets/images/upload_img.png")}/>
             </TouchableOpacity>
           </View>

           <View>
             <TouchableOpacity>
                <Image source={require("../assets/images/upload_vid.png")}/>
             </TouchableOpacity>
          </View>

        </View>
          

        {/* <RadioButtonRN
            
            data={data_type}
            selectedBtn={(e) => {
              console.log(e.label)
              if(e.label == 'Image')
                setType('image/jpeg')
              if(e.label == 'Video')
                setType('video/mp4')
            }}
        /> */}

        {filePath.uri ? (
          <>
            <Image  source={require('../assets/images/ic_search_normal.png')}/>
            {/* <Text>{filePath.uri}</Text> */}
            
            <Image
              source={{uri: filePath.uri}}
              style={styles.imageStyle}
            />
            <TouchableOpacity
              activeOpacity={0.5}
              style={styles.buttonStyleGreen}
              onPress={uploadFile}>

              <Text style={styles.textStyleWhite}>
                Upload {(type=="image/jpeg")?"Image":"Video"}
              </Text>
            </TouchableOpacity>
          </>
        ) : null}


        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.buttonStyle}
          onPress={chooseFile}
          
          >
          <Text style={styles.textStyleWhite}>
            Choose {(type=="image/jpeg")?"Image":"Video"}
          </Text>
        </TouchableOpacity>
        </View>
        </ScrollView>
          </View>
      // </ScrollView>

  );
};

export default Loopingupload;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    //#fff 
    // #E5E5E5
    backgroundColor: '#F2F8FF',
  },
  titleText: {
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 20,
  },
  textStyle: {
    padding: 10,
    color: 'black',
    textAlign: 'center',
  },
  textStyleGreen: {
    padding: 10,
    color: 'green',
  },
  textStyleWhite: {
    padding: 15,
    color: '#FFFFFF',
  },
  // uploadImgStyle: {

  // },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#0E4C75',
    // marginVertical: 40,
    // width: '100%',
    borderRadius: 8,
    marginHorizontal:20,
    bottom:hp("60%")
    // bottom:90
    // top: 5,
  },
  buttonStyleGreen: {
    alignItems: 'center',
    backgroundColor: 'green',
    marginVertical: 20,
    width: '100%',
  },
  imageStyle: {
    flex: 1,
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    margin: 5,
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    color:"#000000",
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  },
});


const styles4=StyleSheet.create({
  container: {
    
    
    paddingHorizontal: 24,
    paddingBottom:  18,
    
  },
  logoApp: {
    marginTop: 50,
    alignSelf: "center",
    alignItems: "center",
  },
  logo: {
    height:100,
    width:100,
    marginBottom: 12,
    
  },
  inputLogin:{
    marginTop:hp("4%"),
    borderRadius:1,
    marginHorizontal:wp("5%")
  }

})
