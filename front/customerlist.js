import axios from 'axios';
import React,{useState,useEffect, useContext} from 'react';
import PTRView from 'react-native-pull-to-refresh';
import AppContext from '../../AppContext';
//import { SafeAreaView, StyleSheet, TextInput } from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { Authheader } from '../../keys';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Button,
    Text,
    TextInput,
    View,
    Linking,
    TouchableOpacity,
    Image
  } from 'react-native'; 

export const customerlist = ({navigation}) => {
    const myContext=useContext(AppContext);
    const [customers_list,set_customers_list]=useState([]);
    const [search_phrase,set_search_phrase] = useState("");

    const updatedb=()=>{
        axios.get("http://35.154.70.53:5000/order/update_db",Authheader)
        .then(res=>{
            console.log(res.data.msg);
        })
        .catch(err=>{
            console.log(err);
        })

    }


    const fetch_customer_details = async () => {
        console.log("sending backened req");
        axios.get(`http://35.154.70.53:5000/customer/fetch?community_id=${myContext.community}`,Authheader)
        .then(res =>{
            //console.log("Axios response received data: ",res.data);
            set_customers_list([...res.data]);
            console.log("backend req completed");

        }).catch(err => {
            if(err.response)
            {   if (error.response.data) {
                console.log(error.response.data.errors);
                error.response.data.errors.map((e) => {
                   
                   console.log("debug e is ", e);
                   return e;
                }); }
                else{
                    console.log("Network error");
                }
            }
            else if (err.request){
                console.log("2 error->", err.message);
            }
            else {
                // Something happened in setting up the request and triggered an Error
                console.log('e Error is ', err.message);
              }
            
        })
    }
    
    
   
    
    useEffect( () => {
        const unsubscribe = navigation.addListener('focus', () => {
             fetch_customer_details();
          });
          return unsubscribe;
        }, [navigation]);
    
        /* onPress={()=>{Linking.openURL("https://wa.me/91"+customer.mobile+"?text=reminder%0a%0aHello "+customer.name+" garu.%0aBalance pending to be paid is "+customer.balance*-1+".%0aThank you.%0aFrom Chotu")}} */
        /* button onPress={() => {navigation.navigate('Ledger',{customer_mob:customer.mobile,customer_name:customer.name})} } */
   const  _refresh =  ()=> {
        return new Promise((resolve) => {
           fetch_customer_details()
           .then(r => resolve())
          
        });
      }
  
   const compare= ((a,b) =>{
    if(a.balance > b.balance)
    return 1
    else
    return -1
   })


    return (
       
        <PTRView onRefresh={_refresh} style={{backgroundColor:"#F2F8FF"}}>
    
    <ScrollView  style={{marginLeft:wp("4%"),marginTop:hp("6%"),marginRight:wp("3%")}}>
        <View>
            <View style={styles4.inputLogin,{marginTop:5,marginBottom:30}}>
                <View
                style={{borderColor:"#F0F0F0"}}
                activeOpacity={0.7}
                >
                  
                  <View style={{flexDirection:"row"}}>

                  <TextInput
                   placeholder="Search Contact"

                   placeholderTextColor='#000000'
                   value={search_phrase}
                   onChangeText={e=>set_search_phrase(e)}
                  
                style={{flex:1,backgroundColor:'#FFFFFF',borderRadius:8,borderWidth:1,fontFamily:"Muli_600SemiBold",fontSize:15,borderColor:"#F0F0F0",color:"#000000",paddingLeft:20}}
                  borderColor="#F0F0F0"
                  editable={true}>

                  </TextInput >
                  <TouchableOpacity  style={{marginTop:12,right:20,position:'absolute'}}>
                  <Image  source={require('../assets/images/ic_search_normal.png')}/>
                  </TouchableOpacity>
                  </View>
                </View>
            </View>
       {/* <TextInput
        style={styles.input}
        onChangeText={(e) => set_search_num(e)}
        value={search_num}
        placeholder="Mobile"
        keyboardType="numeric"

        />  */}
        
        
         {customers_list.sort(compare).map((customer,indx) => {
           
             if (customer.name.toLowerCase().includes(search_phrase.toLowerCase()) || customer.mobile.includes(search_phrase))
              {  
                if(true){
                if(customer.balance<0){ 
                  return(
            
                <TouchableOpacity key={indx} style={styles5.container} button onPress={() => {navigation.navigate('Ledger',{customer_mob:customer.mobile,customer_name:customer.name})} }>
                  <TouchableOpacity onPress={()=>{Linking.openURL("https://wa.me/91"+customer.mobile+"?text=reminder%0a%0aHello "+customer.name+" garu.%0aBalance pending to be paid is "+customer.balance*-1+".%0aThank you.%0aFrom Chotu")}}>
               {/* style={{width:30,height:30}} */}
                <Image  source={require('../assets/images/feed_img.png')}/>
                </TouchableOpacity>
                {/* style={{marginLeft:20,width:200}} */}
                <View style={{bottom:hp("0.3%")}}> 

                  <Text  style={{fontSize:22,fontWeight:'bold', color:"#0E4C75",}}>{(customer.name).length < 13 ? customer.name : (customer.name).substring(0, 12)}</Text>
                  <Text style={{fontSize:11,color:'#0E4C75'}}>{customer.mobile}</Text>
                  
                  {/* <Image  source={require('../assets/images/phone_icon.png')}/>{" "} */}
                  
                 
                </View>
              {/* paddingHorizontal:20,  */}
                <View style={{ backgroundColor:'#FFE9E9', borderRadius:20,position:'absolute',width: wp("26%"),height:hp("4.3%"),marginLeft:wp("60%")}}>
                  <Text style={{textAlign:'center', color:'#EB0000',fontWeight:'bold',top:hp("1%")}}>- ₹{-1*customer.balance}</Text>
                  
                </View>
            
                {/* <View style={{flexDirection:'row'}}>
                <Text style={{fontSize:15,color:'red',fontWeight:'bold'}}>{customer.balance}</Text>
                
                </View> */}
                {/* <Image style={{width:15,height:15,position:'absolute',right:25}} source={require('../assets/images/singlesmall.png')}/> */}
                </TouchableOpacity>
           
         
                )
               }
               else 
               {
                 return(
                  <TouchableOpacity key={indx} style={styles5.container} button onPress={() => {navigation.navigate('Ledger',{customer_mob:customer.mobile,customer_name:customer.name})} }>
                    <TouchableOpacity >
                    {/* style={{width:30,height:30}} */}
                    <Image  source={require('../assets/images/feed_img.png')}/>
                    </TouchableOpacity>
                    {/* style={{marginLeft:20,width:200}} */}
                    <View style={{bottom:hp("0.3%")}}>
                      <Text style={{fontSize:22,fontWeight:'bold', color:"#0E4C75"}}>{(customer.name).length < 11 ? customer.name : (customer.name).substring(0, 10)}</Text>
                      <Text style={{fontSize:11,fontWeight:'bold',color:'#0E4C75'}}>{customer.mobile}</Text>
                      
                    </View>
                    {/* marginLeft: wp("10%"), */}
                    <View style={{ backgroundColor:'#E5FFE5', borderRadius:25,position:'absolute',width: wp("26%"),height:hp("4.3%"),marginLeft:wp("60%")}}>
                    <Text style={{textAlign:'center', color:'#08C908',fontWeight:'bold', top:hp("1%")}}>+{customer.balance}</Text>
                    
                    </View>
                    {/* <Image style={{width:15,height:15,position:'absolute',right:25}} source={require('../assets/images/singlesmall.png')}/> */}
                  </TouchableOpacity>
             
                 )

               
            }
        }
       
    }
            else 
            return null;
        
        
        }
            
            )} 
             
        

             </View>          
        </ScrollView>
        
      </PTRView>
     
        
        
    );
}





const styles = StyleSheet.create({
    header:{
        height:20,
        margin:5

    },
    input: {
        height: 40,
        marginTop:3,
        marginBottom:7,
        marginLeft:7,
        marginRight:7,
        borderBottomWidth:1,
        color:'#0f4c75'
      },

}) 

const styles4=StyleSheet.create({
    container: {
      
      
      paddingHorizontal: 24,
      paddingBottom:  18,
      
    },
    logoApp: {
      marginTop: 50,
      alignSelf: "center",
      alignItems: "center",
    },
    logo: {
      height:100,
      width:100,
      marginBottom: 12,
      
    },
    inputLogin:{
      marginTop:50,
    }

  })

const styles5=StyleSheet.create({
  container:{
    padding:20,
    backgroundColor:'#FFFFFF',
    borderRadius:10,
    marginTop:8,
    marginLeft:2,
    marginRight:2,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "blue",
    shadowRadius: 4,
    elevation: 6,
    marginBottom:16,
  }
})
