import * as React from 'react';
import { useState ,useEffect } from 'react';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { NavigationContainer } from '@react-navigation/native';
import { ActivityIndicator, Alert } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppContext from '../../AppContext';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LottieView from 'lottie-react-native';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TouchableOpacity,
  CheckBox,
  TextInput,

  Image,
  Icon,
  Button,
  View,
} from 'react-native';
import { Picker } from '@react-native-community/picker'
import { color } from 'react-native-reanimated';

import axios from 'axios';
import { Authheader } from '../../keys';
import { registerCustomIconType } from 'react-native-elements';




const Team_approval_pending = ({ route,navigation }) => {
  const myContext = React.useContext(AppContext)
  
  const storeData = async () => {
    try {
      await AsyncStorage.removeItem('captain_id')
      await AsyncStorage.removeItem('captain_mobile')
      await AsyncStorage.removeItem('team_id')
      console.log("removed data")
    } catch (e) {
      console.log('came here')
    }
  }

  const logoutfn=async ()=>{
    await storeData();
    myContext.setloginvalue(false)
  }

  



  return (
    <View >

      {/* <View style={{ marginLeft:24,marginRight:24,marginTop:80,marginBottom:40,borderRadius:5}}>
          
            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
            <Image  source={require('../assets/images/chotu.jpeg')}
            style={{width:100,height:100}}/>
            </View>

            <View style={{flexDirection:"row"}}>
           
             <TextInput
             placeholder="CommunityID"
             value={comid}
             
             onChangeText={e=>set_comid(e)}
        style={styles2.input}
        
        
/>
            </View>

            <View style={{flexDirection:"row"}}>
           
             <TextInput
             placeholder="Password"
             value={Password}
             onChangeText={e=>set_password(e)}
        style={styles2.input}
        />
            </View>
           

             
           
            

            <View style={{marginTop:60,width:150,alignContent:"center",marginLeft:100}}>
            
            <Button  title="submit" color='#0f4c75' onPress={()=>submitfn()} ></Button>
            
            
            </View>
            
        </View> */}
      <View style={{ backgroundColor: "#f2f8ff", height: hp("100%") }}>
     
        
            <View style={styles5.container}>
            <TouchableOpacity  onPress={() => logoutfn()}>
              <Image style={{tintColor:"#000000",marginLeft:wp("70%"),top:hp("1.3%")}} source={require('../assets/images/cross.png')}/>
            </TouchableOpacity>
                 
                 <View style={{ height:hp("25%"),width:("120%"),}}>
                   <LottieView
                     source={require("../assets/images/gif2.json")}
                     size={60}
                     autoPlay
                     loop/>
                 </View>
                
                 <Text style={{ color:"#282C3F", fontWeight:"bold", fontSize:18}}>Your request has placed</Text>
                 <Text style={{color:"#282c3f" ,fontSize:15,width:wp("50%"),textAlign:"center"}}>Your request for create a team has been placed </Text>
    

          
            </View>
            
   

         <TouchableOpacity
            onPress={() => logoutfn()}
            style={{ marginHorizontal:wp("9%"),marginTop: hp("30%"), backgroundColor: "#0f4c75", padding: 15, borderRadius: 8 }}>
            <Text style={{ fontWeight: "bold", color: "#FFFFFF", textAlign: "center", fontSize: 15 }}>Go back</Text>
          </TouchableOpacity>

      </View>
    </View>
  )
}


const styles5=StyleSheet.create({
  container:{
    // flex:1,
    top:hp("18%"),
    // padding:30,
    backgroundColor:'#FFFFFF',
    borderRadius:5,
    // width: wp("50%"),
    height:hp("40%"),
    // marginTop:8,
    // marginLeft:wp("10%"),
    marginHorizontal:wp("10%"),
    // marginRight:2,
    // flexDirection: "row",
    alignItems: "center",
    // justifyContent:"center",
    // shadowColor: "blue",
    shadowRadius: 10,
    elevation: 4,
    // marginBottom:16,

  }
})


const styles2 = StyleSheet.create({
  input: {
    height: 40,
    marginTop: 12,
    marginBottom: 12,
    marginLeft: 6,
    marginRight: 6,
    borderBottomWidth: 2,

    borderColor: '#0f4c75',
    flex: 2.8
  },
  input2: {
    height: 40,
    paddingTop: 8,
    paddingLeft: 8,
    marginTop: 28,
    marginBottom: 12,
    marginLeft: 20,
    marginRight: 6,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#e6005c",
    flex: 2.8
  },
});


const styles = StyleSheet.create({
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  container: {
    flex: 1,
    margin: 10,
    marginTop: 30,
    padding: 30,

  },

  buttonFacebookStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    borderWidth: 0.5,
    borderColor: '#fff',
    height: 40,
    width: 35,
    borderRadius: 5,
    margin: 5,
  },
  buttonImageIconStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
  },
  buttonTextStyle: {
    color: '#fff',
    marginBottom: 4,
    marginLeft: 10,
  },
  buttonIconSeparatorStyle: {
    backgroundColor: '#fff',
    width: 1,
    height: 40,
  },
});

const styles3 = StyleSheet.create({
  container: {
    flex: 4,
    paddingTop: 8,
    alignItems: "center"
  }
});

const styles4 = StyleSheet.create({
  container: {


    paddingHorizontal: 24,
    paddingBottom: 18,

  },
  logoApp: {
    marginTop: 50,
    alignSelf: "center",
    alignItems: "center",
  },
  logo: {
    height: 100,
    width: 100,
    marginBottom: 12,

  },
  inputLogin: {
    marginTop: 50,
  }

})


export default Team_approval_pending;
