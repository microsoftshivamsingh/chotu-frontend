import * as React from 'react';
import { useState } from 'react';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { NavigationContainer } from '@react-navigation/native';
import { ActivityIndicator, Alert } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import AppContext from '../../AppContext';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TouchableOpacity,
  CheckBox,
  TextInput,

  Image,
  Icon,
  Button,
  View,
} from 'react-native';
import { Picker } from '@react-native-community/picker'
import { color } from 'react-native-reanimated';
import {launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import axios from 'axios';
import { Authheader } from '../../keys';

const Cap_register = ({ route,navigation}) => {
  const {cap_number}=route.params
  const myContext = React.useContext(AppContext)
  const [cap_name, setcap_name] = useState("")
  const [cap_number2,setcap_number2] = useState(cap_number)
  const [Password, setPassword] = useState("")
  const [ReenterPassword, setReenter] = useState("")
  const [description, setDescription] = useState("")
  const [filePath, setFilePath] = useState("");
  const [uris,set_uris]=useState("");
  

  /* const chooseFile = () => {
    let options = {
      mediaType: 'photo',
      includeBase64:true,
      
      maxWidth:500,
      maxHeight:500
    
    };
    
    launchImageLibrary(options, (response) => {
      
      
      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      setFilePath(response);
      set_uris(response.assets[0].uri)
      
    });
  }; */

  const chooseFile=()=>{
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      cropping: true,
      includeBase64:true
    }).then(image => {
      set_uris(image.path)
      setFilePath(image.data)
      
    })
    .catch(err=>{
      console.log(err)
    }
    )
  }

  const submitfn = async () => {
    console.log("Entered")
    if(Password == ReenterPassword)
    {
      const authdetails = {
        captain_name:cap_name,
        captain_mobile:'+91' + cap_number2,
        captain_password:Password,
        captain_description:description,
        image:filePath
      }
     
      await axios.post("http://65.1.88.93:5000/captain/add",authdetails,Authheader)
      .then((res)=>{
        if(res.data.msg == "Captain already exists with this mobile number")
        {
          console.log(res.data.msg)
          alert(res.data.msg)
        }
        if(res.data.msg == "Your request has been sent")
        {
          console.log(res.data.msg)
          alert("Your request has been sent");
          navigation.navigate('Login', {})
        }
      })
    }
    if(Password != ReenterPassword)
    {
      alert("Both the pins should be the same");
    }
  }

  return (
    <ScrollView >
    <View >
      <View style={{ backgroundColor: "#F2F8FF", height: hp("120%") }}>
      
        <View style={styles4.container}>
        
          <View style={styles4.inputLogin}>
          <Text style={{ fontSize: 30, color: "black", fontWeight: "bold", color: '#000000',textAlign: "center",}}>
              Captain Details
            </Text>
            <View style={{marginBottom:14,alignItems: "center"}}>
            <TouchableOpacity 
            onPress={()=>chooseFile()}
            style={{height:100,width:100,borderRadius:50}}>
                           <Image style={{height:100,width:100,borderRadius:50,}} source={require('../assets/images/feed_img.png')}/>

            </TouchableOpacity>
         
            </View>
                {
              uris!=""?<View style={{alignItems:'center'}}>

                <Image
                source={{uri:uris}}
                style={{height:100,width:100,borderRadius:50}}
                >
                </Image>
               

                </View>:null
            }




            <TouchableOpacity
              style={{ borderColor: "#F0F0F0", }}
              activeOpacity={0.7}
            >
            
              <TextInput
                
                value={cap_name}
                placeholder="enter your name"
                  placeholderTextColor = "grey"
                onChangeText={e => setcap_name(e)}
                style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}
                editable={true}

              >
              </TextInput>
              
            </TouchableOpacity>
          </View>

          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              
              <TextInput
                value={Password}
                placeholder="enter your phone number"
                  placeholderTextColor = "grey"
                onChangeText={e => setPassword(e)}
                secureTextEntry={true}
                keyboardType="numeric"
                maxLength={4}
                style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}
                editable={true}
              >
              </TextInput>
            </TouchableOpacity>
          </View>
            <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              
              <TextInput
                value={Password}
                placeholder="enter 4-Digit Passcode"
                  placeholderTextColor = "grey"
                onChangeText={e => setPassword(e)}
                secureTextEntry={true}
                keyboardType="numeric"
                maxLength={4}
                style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}
                editable={true}
              >
              </TextInput>
            </TouchableOpacity>
          </View>
          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
             
              <TextInput
                value={ReenterPassword}
                 placeholder="Re-enter 4-Digit Passcode"
                 placeholderTextColor = "grey"
                onChangeText={e => setReenter(e)}
                secureTextEntry={true}
                keyboardType="numeric"
                maxLength={4}
                style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}

                editable={true}

              >

              </TextInput>
            </TouchableOpacity>
          </View>
          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
             
              <TextInput
                value={description}
                placeholder="enter description"
                 placeholderTextColor = "grey"
                onChangeText={e => setDescription(e)}
                style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000', height:150}}
                editable={true}

              >
              </TextInput>
            </TouchableOpacity>
          </View>
          {/* <View style={{marginBottom:10}}>
            <TouchableOpacity 
            onPress={()=>chooseFile()}
            style={{marginTop:50,backgroundColor:"#0f4c75",padding:15,borderRadius:12}}>
                <Text style={{fontWeight:"bold",color:"#FFFFFF",textAlign:"center",fontSize:15}}>Upload-Image</Text>
            </TouchableOpacity>
            </View>

            {
              uris!=""?<View style={{alignItems:'center'}}>

                <Image
                source={{uri:uris}}
                style={{height:100,width:100,borderRadius:50}}
                >
                </Image>
               

                </View>:null
            } */}
          <TouchableOpacity
            onPress={() => submitfn()}
            style={{ marginTop: 50, backgroundColor: "#0f4c75", padding: 15, borderRadius: 8 }}>
            <Text style={{ fontWeight: "bold", color: "#FFFFFF", textAlign: "center", fontSize: 15 }}>Submit</Text>
          </TouchableOpacity>
   
          <TouchableOpacity style={{top: hp("2.9%"),}}>
                 <View   >
                     <Text style={{color:"#000000", fontWeight:"bold",textDecorationLine: 'underline', textAlign: "center",fontSize: 20}}>*If declined*</Text>
                 </View>
           </TouchableOpacity>

            <TouchableOpacity  style={{top: hp("3.9%"),}}>
            <View>
            <Text style={{color:"#000000",fontWeight:"bold",textDecorationLine: 'underline', textAlign: "center",fontSize: 20}}>*team request*</Text>
           </View>
            </TouchableOpacity>
           
        </View>
      </View>
    </View>
    </ScrollView>
  )

}
export default Cap_register;

const styles4 = StyleSheet.create({
  container: {

    top:60,
    paddingHorizontal: 24,
    paddingBottom: 18,

  },
  logoApp: {
    marginTop: 50,
    alignSelf: "center",
    alignItems: "center",
  },
  logo: {
    height: 100,
    width: 100,
    marginBottom: 12,

  },
  inputLogin: {
    marginTop: 30,
  }

})