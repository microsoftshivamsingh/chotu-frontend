import axios from 'axios';
import React,{useState,useEffect, useContext} from 'react';
import PTRView from 'react-native-pull-to-refresh';
import AppContext from '../../AppContext';
//import { SafeAreaView, StyleSheet, TextInput } from "react-native";
import DropDownPicker from 'react-native-dropdown-picker';
import { Authheader } from '../../keys';
import DateTimePicker from '@react-native-community/datetimepicker'; 
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Button,
    Text,
    TextInput,
    View,
    Linking,
    Alert,
    TouchableOpacity,
    Image
  } from 'react-native'; 
import { exp, or } from 'react-native-reanimated';
import { renderNode } from 'react-native-elements/dist/helpers';
import {launchImageLibrary} from 'react-native-image-picker';




const Feedbackform=({route,navigation})=>{
    const myContext=React.useContext(AppContext)
    const { order_id} = route.params;
    const [isorder,set_isorder]=useState(1);
    const [orderdetails,set_orderdetails]=useState({})
    const [elaborate,set_elaborate]=useState("")
    const [show,set_show]=useState(false);
    const [filePath, setFilePath] = useState({});
    const [uris,set_uris]=useState("");
    const [items, setItems] = useState([
        {label: 'Quality is not Good', value: 'Quality is not Good'},
        {label: 'Quantity is less than mentioned', value: 'Quantity is less than mentioned'},
        {label:'Didnot receive some or all items',value:'Didnot receive some or all items'},
        {label:'Late Delivery',value:'Late Delivery'},
        
        {label:'Other',value:'Other'}
        
      ]);
    const [refund_amount,set_refund_amount]=useState("")
    const [value1,set_value1]=useState(null);
    const [open1,set_open1]=useState(false);
    const fetch_order_details=async ()=>{
       await axios.get(`http://35.154.70.53:5000/refund/isorder?order_id=${order_id}`,Authheader)
       .then(res=>{
            console.log(res.data)
            if(!res.data){
              set_show(true)
            }
       })
       .catch(err=>{
         console.log(err);
       })
       
       await axios.get(`http://35.154.70.53:5000/order/fetchonebyid?id=${order_id}`,Authheader)
    .then(res => {
      
      
      
      let temp = []
      set_isorder(2);
      set_orderdetails({...res.data})
      console.log(orderdetails)
      
      
    
    })
    .catch(err=>{
        console.log(err)
    })
    
  }

    const submitfn=()=>{
        const payload={
            order_id:order_id,
            community_id:myContext.community,
            id:"refund_"+order_id,
            phone_no:orderdetails.customer_id,
            refund_amount:refund_amount,
            type_of_reason:value1,
            elaborate_feedback:elaborate,
            name:orderdetails.customer_name,
            vendor_name:orderdetails.vendor_name,
            base:filePath.assets[0].base64,
            date_of_request:Date.now()

        }
        Alert.alert(
            "Feedback",
            "Are you sure want to submit",
            [
              
              { text: "OK", onPress: () => {

                axios.post("http://35.154.70.53:5000/refund/add",payload,Authheader)
                .then(res => {
                Linking.openURL("https://wa.me/91"+payload.phone_no+"?text=REFUND REQUEST%0a%0aHello "+orderdetails.customer_name+" Ji."+"%0aYour refund request with id "+payload.id+" is recorded successfully. You will be contacted soon by Chotu."+"%0a%0aThank You" )
                navigation.navigate('Customers')
                })
                .catch(err=>{
                console.log("error")
                })

              } },
              {text:"Cancel"}
            ]
          );
        
    }

    const chooseFile = () => {
      let options = {
        mediaType: 'photo',
        includeBase64:true,
        
        maxWidth:250,
        maxHeight:500
      
      };
      
      launchImageLibrary(options, (response) => {
        
        
        if (response.didCancel) {
          alert('User cancelled camera picker');
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }
        setFilePath(response);
        set_uris(response.assets[0].uri)
        
      });
    };

    useEffect( () => {
        const unsubscribe = navigation.addListener('focus', () => {
             fetch_order_details();
          });
          return 
        }, [navigation]);
      
      
      
      const _refresh =  ()=> {
        return new Promise((resolve) => {
           fetch_order_details()
           .then(r => resolve())
          
        });
      }



    if(show){

    
    if(orderdetails !={}){
    console.log(orderdetails.invoice)
    if(true){

    
    
    return(
        <ScrollView nestedScrollEnabled={true} style={{backgroundColor:'#FAFAFA',height:'100%'}}>
        <View style={{marginLeft:20,marginRight:20}}>
            <View style={{flexDirection:'row'}}>
            <View style={{marginTop:20,marginBottom:20,flex:2}}>
                <View style={{flexDirection:'row'}}>
                <Text style={{color:'#0f4c75',fontSize:15,fontWeight:'bold'}}>Name: </Text>
                <Text style={{color:'#676b6b',fontSize:15,fontWeight:'bold'}}>{orderdetails.customer_name}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                <Text style={{color:'#0f4c75',fontSize:15,fontWeight:'bold'}}>PhoneNo: </Text>
                <Text style={{color:'#676b6b',fontSize:15,fontWeight:'bold'}}>{orderdetails.customer_id}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                <Text style={{color:'#0f4c75',fontSize:15,fontWeight:'bold'}}>Ordered Amount: </Text>
                <Text style={{color:'#676b6b',fontSize:15,fontWeight:'bold'}}>{orderdetails.total}</Text>
                </View>
            </View>
            <View style={{marginTop:20}}>
                <TouchableOpacity style={styles5.container} onPress={() => {navigation.navigate('OrderDetails',{order_id:orderdetails._id})}}>
                    <Text style={{width:50,textAlign:'center',fontWeight:'bold',color:'#0f4c75'}}>Order Details</Text>
                </TouchableOpacity>
            </View>

            </View>
            <View style={styles4.inputLogin}>
                <TouchableOpacity
                style={{borderColor:"#F0F0F0"}}
                activeOpacity={0.7}
                >
                  <Text style={{fontSize:15,color: "black",fontWeight:"bold",color:"#0f4c75"}}>
                  Refund Amount Requested
                  </Text>
                  <TextInput
                  keyboardType="number-pad"
                  value={refund_amount}
                  onChangeText={e=>set_refund_amount(e)}
                  style={{backgroundColor:'#FFFFFF',borderRadius:8,borderWidth:1,marginTop:4,fontSize:15,borderColor:"#F0F0F0"}}
                  
                  editable={true}

                  >

                  </TextInput>
                </TouchableOpacity>
            </View>
            <View>
            <Text style={{fontSize:15,marginBottom:2,fontWeight:"bold",color:"#0f4c75"}}>Reason for Request</Text>
            <DropDownPicker
                
                open={open1}
                value={value1}
                placeholder="Select reason"
                items={items}
                style={{height:40}}
                listMode="SCROLLVIEW"
                setOpen={set_open1}
                setValue={set_value1}
                setItems={setItems}
                    
            />
            </View>

            <View style={styles4.inputLogin}>
                <TouchableOpacity
                style={{borderColor:"#F0F0F0"}}
                activeOpacity={0.7}
                >
                  <Text style={{fontSize:15,color: "black",fontWeight:"600",fontWeight:"bold",color:"#0f4c75"}}>
                  Elaborate
                  </Text>
                  <TextInput
                  value={elaborate}
                  onChangeText={e=>set_elaborate(e)}
                  style={{backgroundColor:'#FFFFFF',borderWidth:1,marginTop:4,fontSize:15,borderColor:"black"}}
                  multiline={true}
                  editable={true}

                  >

                  </TextInput>
                </TouchableOpacity>
            </View>
            <View style={{marginBottom:20}}>
            <TouchableOpacity 
            onPress={()=>chooseFile()}
            style={{marginTop:10,backgroundColor:"#0f4c75",padding:15,borderRadius:12}}>
                <Text style={{fontWeight:"bold",color:"#FFFFFF",textAlign:"center",fontSize:15}}>UploadImage</Text>
            </TouchableOpacity>
            </View>
            <View>

            
            
            </View>
            {
              uris!=""?<View style={{alignItems:'center'}}>

                <Image
                source={{uri:uris}}
                style={{height:100,width:100}}
                >
                </Image>
               

                </View>:null
            }
            <View>
              
            <TouchableOpacity 
            onPress={()=>submitfn()}
            style={{backgroundColor:"#0f4c75",padding:15,borderRadius:12}}>
                <Text style={{fontWeight:"bold",color:"#FFFFFF",textAlign:"center",fontSize:15}}>Submit</Text>
            </TouchableOpacity>
            </View>
        </View>
        </ScrollView>

    )
    }
    else{
        return(
        
            <View style={{backgroundColor:"#FAFAFA",height:"100%",PaddingTop:20,padding:20}}>
                <Text>Please complete the delivery to make a refund request. If no produts are delivered complete delivery with invoice amount 0.</Text>
            </View>
        )
    }
    }
    else{
        return null;
    }
  }
  else{
    return(
      // <View>
      //   <Text>
      //     Your refund Request is already recorded you will be contacted soon.
      //   </Text>
      // </View>
      <ScrollView nestedScrollEnabled={true}>
      <View>
        <View style={{backgroundColor:"#F2F8FF",height:hp('100%')}}>
      
        <View style={{backgroundColor:"#0E4C75",flexDirection:'row',height:hp("30%"),width:wp("100%"),justifyContent:"center",textAlign:'center'}}>
            <View style={{justifyContent:"center",textAlign:'center'}}>
                <Image style={{width:wp("44%"),height:hp("18%"),justifyContent:"center",}} source={require('../assets/images/feed_img.png')}/>
                <View style={{bottom:hp("4.5%")}}>
                <Text style ={{fontSize:wp("5%"),fontWeight:'bold',color:'#FFFFFF',textAlign:'center',}}>sdvsdvsdv</Text>
                <Text style ={{fontSize:wp("3%"),color:'#FFFFFF',textAlign:'center',}}>sdvsdvdvs</Text>
                </View>
            </View>
        </View>
        <View style={{ marginLeft:wp("6%"),marginRight:wp("6%"),bottom:hp("2%")}}>
           <View style={{top:hp("5%"),left:wp("1%")}}>
           <Text style={{textAlign:"left",fontSize:wp("4.5%"),color:'#0f4c75', }}>Refund Amount</Text>
           </View>
           
           
          
                
            <View style={{flexDirection:"row",marginTop: hp("5.5%"),}}>
           
             <TextInput
             keyboardType="numeric"
             style={styles2.input}
               color="black"/>

            </View>
              <View style={{flexDirection:"row",marginTop: hp("1.5%"),}}>
           
             <TextInput
            //  keyboardType="numeric"
               placeholder='Reason For Return'
              placeholderTextColor="black"
             style={styles2.input}
             color="black"/> 
           
       

            </View>

            <View style={{top:hp("1%"),left:wp("1%"),}}>
           <Text style={{textAlign:"left",fontSize:wp("4.5%"),color:'#0f4c75',fontWeight:'bold'}}>Additional details</Text>
           </View>


            <View style={{flexDirection:"row",marginTop: hp("1.5%"),height: hp("4.5%"),}}>
           
             <TextInput
              multiline
              editable
              maxLength={100}
              
            //  numberOfLines={50}
             style={{
                  height: hp("11.5%"),
                  borderWidth: 1,
                  borderRadius:8,
                  borderColor:"#0f4c75",
                  flex:1,
                  backgroundColor: '#FFFFFF',
             }}
             color="black"/>
            
   

            </View>
            
             <View style={{top:hp("8.5%"),}}>
             <TouchableOpacity>
                <Image style={{height:hp("22%"),width:wp("90%"),}} source={require("../assets/images/upload_pic.png")}/>
             </TouchableOpacity>
           </View>
           
               <TouchableOpacity 
            onPress={() => { navigation.navigate('Refundform', { order_id:a._id }) }}
            style={{top:hp("10%"),backgroundColor:"#0f4c75",padding:15,borderRadius:12}}>
                <Text style={{fontWeight:"bold",color:"#FFFFFF",textAlign:"center",fontSize:15}}>Request Refund</Text>
            </TouchableOpacity>
            
            </View>
       </View>
        </View>
        </ScrollView>
    )
  }

}

export default Feedbackform;

const styles = StyleSheet.create({
    
    container: {
      flex: 1,
      margin: 10,
      marginTop: 30,
      padding: 30,
      
    },
   

    date:{
        // textAlignVertical:"center",
        borderBottomWidth:1,
        borderTopWidth:1,
        borderRightWidth:1,
        borderBottomRightRadius:8,
        borderTopRightRadius:8,
        // borderColor:"#0f4c75",
        textAlign:'center',
        paddingTop:wp("3.4%"),
        height: hp("5.5%"),
        // right:wp("10%"),
        // height:39,
        // padding: 10,
  
        // justifyContent:'center',
        // position:"relative",
        marginLeft:wp("-5%"),
        left:wp("5%"),
        paddingRight:wp("10%"),
        flex:1
    },
     amount:{
        backgroundColor:"#0E4C75",
        borderTopWidth:2,
        //  flexDirection: 'row',
        // flex:0.2,
        // borderBottomWidth:5,
        borderBottomLeftRadius:8,
        borderTopLeftRadius:8,
        borderLeftWidth:2,
      
        top:52.6,
        paddingBottom:29,
        marginTop:1,
        // marginLeft:10,
        paddingTop:10,
        paddingLeft:23,
        width:60,
        height: 40,
        left:2,
        flexDirection: 'row',
        borderColor:"#0f4c75",
        // margin: 1,
    },
    buttonFacebookStyle: {
      backgroundColor:"#0E4C75",
      borderBottomWidth:2,
      borderTopWidth:2,
      flexDirection: 'row',
      // alignItems: 'center',
      flex:0.3,
      marginTop:hp("-0.1%"),
      borderLeftWidth:2,
      borderColor:"#0f4c75",
      height: hp("5.6%"),
     borderBottomLeftRadius:8,
     borderTopLeftRadius:8,
      margin: wp("-0.1%"),
      // marginRight:10
      // width:1000,
    },
    buttonImageIconStyle: {
      // padding: 8,
      marginLeft:wp("6%"),
      marginTop:hp("1.4%"),
      height: hp("2.3%"),
      width: wp("5%"),
      resizeMode: 'stretch',
    },
    buttonTextStyle: {
      color: '#fff',
      marginBottom: 4,
      marginLeft: 10,
    },
    buttonIconSeparatorStyle: {
      backgroundColor: '#000000',
      width: 1,
      height: 40,
    },
  });

const styles2 = StyleSheet.create({
    input: {
      height: hp("5.5%"),
      marginBottom:hp("1.5%"),
      borderWidth: 1,
      borderRadius:8,
      borderColor:"#0f4c75",
      borderBottomWidth: 1,
      flex:1,
      backgroundColor: '#FFFFFF',
     
    },
  });

const styles5=StyleSheet.create({
    container:{
      padding:20,
      backgroundColor:'#FFFFFF',
      borderRadius:16,
      
      flexDirection: "row",
      
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.05,
      shadowRadius: 4,
      elevation: 4,
      
      marginBottom:16,
    }
  })


  const styles4=StyleSheet.create({
    container: {
      
      alignItems:'center',
      alignSelf:'center',
      paddingHorizontal: 24,
      paddingBottom:  18,
      
    },
    logoApp: {
      marginTop: 50,
      
      
    },
    logo: {
      height:100,
      width:100,
      marginBottom: 12,
      
    },
    inputLogin:{
      marginTop:10,
      marginBottom:10
    },
    imageStyle: {
      flex: 1,
      width: 50,
      height: 50,
      resizeMode: 'contain',
      margin: 5,
    },

  })

