import * as React from 'react';
import { useState } from 'react';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { NavigationContainer } from '@react-navigation/native';
import { ActivityIndicator, Alert } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppContext from '../../AppContext';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TouchableOpacity,
  CheckBox,
  TextInput,

  Image,
  Icon,
  Button,
  View,
} from 'react-native';
import { Picker } from '@react-native-community/picker'
import { color } from 'react-native-reanimated';

import axios from 'axios';
import { Authheader } from '../../keys';
import { registerCustomIconType } from 'react-native-elements';
import LottieView from 'lottie-react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';




const Login = ({ route,navigation }) => {
  const myContext = React.useContext(AppContext)
  
  const [cap_number, setcap_number] = useState("")
  const [Password, set_password] = useState("")
  const [hidepassword, set_hidepassword] = useState(true)
  

  const storeData2 = async (captain_mobile,captain_id) => {
    try {
      await AsyncStorage.setItem('captain_mobile', captain_mobile)
      await AsyncStorage.setItem('captain_id', captain_id)
      console.log("stored data")
    } catch (e) {
      console.log('error in async')
      // saving error
    }
  }

  
  const register = (() => {
    navigation.navigate('Verification', {})
  })


  const submitfn = (() => {

    
    const authdetails = {
      captain_mobile: "+91"+cap_number,
      captain_password: Password
    }
    
    axios.post("http://65.1.88.93:5000/captain/login", authdetails, Authheader)
      .then(async res => {
        
        if (res.data.msg == "Yes") {

          //await storeData1(res.data.details.community_id);
          await storeData2(res.data.details.captain_mobile,res.data.details.captain_id);
          const authdetails ={
            captain_id:res.data.details.captain_id
          }


          await axios.post("http://65.1.88.93:5000/team/set_status",authdetails,Authheader)
          .then(res=>{
            console.log("Response for team status " + res.data.msg);
            if(res.data.msg == "No team created")
            {
              myContext.set_team_created(false)
              myContext.set_team_approved(false)
            }
            if(res.data.msg == "Your request to create a team is not yet approved")
            {
              myContext.set_team_created(true)
              myContext.set_team_approved(false)
            }
            if(res.data.msg == "Your team is now approved")
            {
              myContext.set_team_created(true)
              myContext.set_team_approved(true)
              myContext.setteam_id(res.data.details.team_id)
            }
          })
          //myContext.setcomname(res.data.details.community_id);
          myContext.setcaptain_mobile("+91"+cap_number);
          myContext.setcaptain_id(res.data.details.captain_id)
          myContext.setloginvalue(true);

          //myContext.set_team_approved(false);
          //console.log(myContext.community)
        }
        else if(res.data.msg == "The user is not yet approved")
        {
          alert(res.data.msg)
        }
        else {
          Alert.alert(
            "Login Failed",
            "communityID or Password is incorrect",
            [

              { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
          );
        }


      })
      .catch(err => {
        console.log(err);
        Alert.alert(
          "Login Failed",
          "Check your internet",
          [

            { text: "OK", onPress: () => console.log("OK Pressed") }
          ]
        );
      })
  })
  return (
    <View >

      {/* <View style={{ marginLeft:24,marginRight:24,marginTop:80,marginBottom:40,borderRadius:5}}>
          
            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
            <Image  source={require('../assets/images/chotu.jpeg')}
            style={{width:100,height:100}}/>
            </View>

            <View style={{flexDirection:"row"}}>
           
             <TextInput
             placeholder="CommunityID"
             value={comid}
             
             onChangeText={e=>set_comid(e)}
        style={styles2.input}
        
        
/>
            </View>

            <View style={{flexDirection:"row"}}>
           
             <TextInput
             placeholder="Password"
             value={Password}
             onChangeText={e=>set_password(e)}
        style={styles2.input}
        />
            </View>
           

             
           
            

            <View style={{marginTop:60,width:150,alignContent:"center",marginLeft:100}}>
            
            <Button  title="submit" color='#0f4c75' onPress={()=>submitfn()} ></Button>
            
            
            </View>
            
        </View> */}
      <View style={{ backgroundColor: "#F2F8FF", height: "100%" }}>
        <View style={styles4.container}>
          <View style={styles4.logoApp}>
            {/* <Image source={require('../assets/images/chotu.jpeg')} style={styles4.logo}>

            </Image> */}
            <Text style={{ fontSize: 30, color: "black", fontWeight: "bold", color: '#000000' }}>
              Login Account
            </Text>
          </View>

          <View style={{ height:hp("20%"),width:wp("40%"),left:wp("25%"), }}>
             <LottieView
                source={require("../assets/images/gif1.json")}
                size={68}
                autoPlay
                loop

              />
          </View>  


          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              {/* <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                Mobile Number
              </Text> */}
              <View style={{ flexDirection: "row" }}>
              {/* <TextInput
   
                  color="black"
                  value={"+91"}
                  style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}
                  editable={false}
                >
                </TextInput> */}
              <TextInput
                value={cap_number}
                placeholder="enter your number"
                placeholderTextColor = "grey"
                keyboardType="numeric"
                onChangeText={e => setcap_number(e)}
                style={{ flex: 1, backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, fontFamily: "Muli_600SemiBold", fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}

                editable={true}

              >

              </TextInput>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles4.inputLogin, { marginTop: 20 }}>
            <View
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              {/* <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                4-Digit Passcode
              </Text> */}
              <View style={{ flexDirection: "row" }}>
                <TextInput
                  value={Password}
                  keyboardType="numeric"
                  placeholder="enter your passcode"
                  placeholderTextColor = "grey"
                  onChangeText={e => set_password(e)}
                  secureTextEntry={hidepassword}
                  style={{ flex: 1, backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, fontFamily: "Muli_600SemiBold", fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}
                  borderColor="#F0F0F0"
                  editable={true}

                >

                </TextInput >
                <TouchableOpacity onPress={() => { set_hidepassword(hidepassword ? false : true) }} style={{ marginTop: 15, right: 20, position: 'absolute' }}>
                  <Image  style={{tintColor: "#000000",resizeMode: "contain"}} source={require('../assets/images/ic_eye_on.png')} />
                </TouchableOpacity>
              </View>
              
            </View>
             
          </View>
              <TouchableOpacity>
            <Text style={{color:"#000000", top: hp("2.9%"),fontWeight:"bold"}}>Contact Support?</Text>
            </TouchableOpacity>

          <TouchableOpacity
            onPress={() => submitfn()}
            style={{ marginTop: 40, backgroundColor: "#0f4c75", padding: 15, borderRadius: 12 }}>
            <Text style={{ fontWeight: "bold", color: "#FFFFFF", textAlign: "center", fontSize: 15 }}>Log In</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => register()}
            style={{ marginTop: 40, backgroundColor: "#0f4c75", padding: 15, borderRadius: 12 }}>
            <Text style={{ fontWeight: "bold", color: "#FFFFFF", textAlign: "center", fontSize: 15 }}>New user? Sign up</Text>
          </TouchableOpacity>

        </View>

      </View>
    </View>
  )
}





const styles2 = StyleSheet.create({
  input: {
    height: 40,
    marginTop: 12,
    marginBottom: 12,
    marginLeft: 6,
    marginRight: 6,
    borderBottomWidth: 2,

    borderColor: '#0f4c75',
    flex: 2.8
  },
  input2: {
    height: 40,
    paddingTop: 8,
    paddingLeft: 8,
    marginTop: 28,
    marginBottom: 12,
    marginLeft: 20,
    marginRight: 6,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#e6005c",
    flex: 2.8
  },
});


const styles = StyleSheet.create({
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  container: {
    flex: 1,
    margin: 10,
    marginTop: 30,
    padding: 30,

  },

  buttonFacebookStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    borderWidth: 0.5,
    borderColor: '#fff',
    height: 40,
    width: 35,
    borderRadius: 5,
    margin: 5,
  },
  buttonImageIconStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
  },
  buttonTextStyle: {
    color: '#fff',
    marginBottom: 4,
    marginLeft: 10,
  },
  buttonIconSeparatorStyle: {
    backgroundColor: '#fff',
    width: 1,
    height: 40,
  },
});

const styles3 = StyleSheet.create({
  container: {
    flex: 4,
    paddingTop: 8,
    alignItems: "center"
  }
});

const styles4 = StyleSheet.create({
  container: {


    paddingHorizontal: 24,
    paddingBottom: 18,

  },
  logoApp: {
    marginTop: 50,
    alignSelf: "center",
    alignItems: "center",
  },
  logo: {
    height: 100,
    width: 100,
    marginBottom: 12,

  },
  inputLogin: {
    marginTop: 50,
  }

})

export default Login;