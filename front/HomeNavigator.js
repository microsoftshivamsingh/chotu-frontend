import React, { useContext } from 'react';

import {createStackNavigator} from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Button, Text, View } from 'react-native';

import Ledger from '../screens/ledger';
import PendingOrders from '../screens/pendingorders'
import Scre from '../screens/register';
import Indipending from '../screens/pending'
import Editpending from '../screens/editpending'
import { customerlist } from '../screens/customerlist';
import Allpins from '../screens/pins';
import PinEditpending from '../screens/pineditpending';
import AppContext from '../../AppContext';
import Pinpending from '../screens/pinpending';
import Vendorpins from '../screens/vendorpins';
import Homeroot from '../screens/home';
import ListVendors from '../screens/Vendors'
import Pinbought from '../screens/Pinbought'
import Pinnotbought from '../screens/Pinnotbought';
import Invite from '../screens/invite'
import Peopleboughtinvendor from '../screens/Peopleboughtinvendor';
import Peoplenotboughtinvendor from '../screens/peoplenotboughtinvendor';
import Editprofile from '../screens/editprofile';
import Poster  from '../screens/poster';
import Posterfill from '../screens/posterfill';
import Layout1 from '../screens/layouts/layout1';
import GeneratePoster from '../screens/generateposter';
import Feedbackmain from '../screens/feedbackmain';
import Feedbackorders from '../screens/feedbackorders';
import Feedbackform from '../screens/feedbackform';
import Orderdetails from '../screens/feedback_details';
import Loopingmain from '../screens/loopingmain';
import Loopingupload from '../screens/loopingupload';
const Tab = createBottomTabNavigator();


const options2={
        
  headerStyle: {
    //'#0f4c75'
    backgroundColor: '#F2F8FF',
  },
  // '#fdcb9e'
  headerTintColor: '#0E4C75',
  headerTitleStyle: {
   
  },
}

const ProfileDrawer=createDrawerNavigator();

const ProfileDrawerScreen=()=>{
  return (
    <ProfileDrawer.Navigator>
      <ProfileDrawer.Screen options={options2} name="Home" component={Homeroot}/>
      <ProfileDrawer.Screen options={options2} name="Invite" component={Invite}/>
      <ProfileDrawer.Screen options={options2} name="Edit Profile" component={Editprofile}/>
      
    </ProfileDrawer.Navigator>
  )
}

const ProfileStack = createStackNavigator();
const ProfileStackScreen = () =>{
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen options={options2} name="Profile" component={Profileroot}/>
      <ProfileStack.Screen options={options2} name="Invite" component={Invite}/>
    </ProfileStack.Navigator>
  );
}


const LoopingStack = createStackNavigator();
const LoopingStackScreen = () => {
  return (
    <LoopingStack.Navigator>
      <LoopingStack.Screen options={options2} name="Feedback Customers" component={Loopingmain} />
      <LoopingStack.Screen options={options2} name="Upload" component={Loopingupload} />
    </LoopingStack.Navigator>
  );
}

const PendingStack = createStackNavigator();
const PendingStackScreen = () =>{
  return (
    <PendingStack.Navigator>
      <PendingStack.Screen options={options2} name="Pins" component={Allpins}/>
      <PendingStack.Screen options={options2} name="pinpending" component={Pinpending}/>
      <PendingStack.Screen options={options2} name="editpending" component={PinEditpending}/>
    </PendingStack.Navigator>
  );
}

const FeedbackStack = createStackNavigator();
const FeedbackStackScreen = () =>{
  return (
    <FeedbackStack.Navigator>
      <FeedbackStack.Screen options={options2} name="Customers" component={Feedbackmain}/>
      <FeedbackStack.Screen options={options2} name="Orders" component={Feedbackorders}/>
      <FeedbackStack.Screen options={options2} name="Refundform" component={Feedbackform}/>
      <FeedbackStack.Screen options={options2} name="OrderDetails" component={Orderdetails}/>
    </FeedbackStack.Navigator>
  );
}

const PosterStack=createStackNavigator();
const PosterStackScreen=()=>{
  return(
  <PosterStack.Navigator>
       <PosterStack.Screen options={options2} name="Poster" component={Poster}/>
       <PosterStack.Screen options={options2} name="PosterFill" component={Posterfill}/>
       <PosterStack.Screen options={options2} name="GeneratePoster" component={GeneratePoster}/>
       <PosterStack.Screen options={options2} name="Layout1" component={Layout1}/>
    </PosterStack.Navigator>
  )
}

const CrmStack=createStackNavigator();
const CrmStackScreen=()=>{
  return(
  <CrmStack.Navigator>
    <CrmStack.Screen options={options2} name="Vendors" component={ListVendors}/>
    <CrmStack.Screen options={options2} name="PINs" component={Vendorpins}/>
    <CrmStack.Screen options={options2} name="PeopleBought" component={Pinbought}/>
    <CrmStack.Screen options={options2} name="PeoplenotBought" component={Pinnotbought}/>
    <CrmStack.Screen options={options2} name="Peopleboughtinvendor" component={Peopleboughtinvendor}/>
    <CrmStack.Screen options={options2} name="Peoplenotboughtinvendor" component={Peoplenotboughtinvendor}/>
  </CrmStack.Navigator>
  )
}
const ReportStack = createStackNavigator();
const ReportStackScreen = () =>{
  return(
    <ReportStack.Navigator>
      <ReportStack.Screen 
      options={options2}
      name="CustomerList" component={customerlist} />
      <ReportStack.Screen 
      options={options2} name="Ledger" component={Ledger} />     
      <ReportStack.Screen options={options2} name="paymentform" component={Scre}/> 
      <ReportStack.Screen options={options2} name="indipending" component={Indipending}/>
      <ReportStack.Screen options={options2} name="editpending" component={Editpending}/>

    </ReportStack.Navigator>
  );
}





export const HomeNavigator = () => {

    return (    
    <Tab.Navigator tabBarOptions={{
      activeTintColor: '#0f4c75',
      inactiveTintColor: 'lightgray',
      activeBackgroundColor: '#fff',
      inactiveBackgroundColor: '#0f4c75',
      labelStyle:{fontSize:15,marginBottom:10},
          
   }}

    >
      <Tab.Screen  name = "Profile" component ={ProfileDrawerScreen} />
      <Tab.Screen   name = "Delivery" component ={PendingStackScreen} />
      <Tab.Screen   name = "KhataBook" component ={ReportStackScreen} />
      <Tab.Screen   name = "CRM" component ={CrmStackScreen} />
      <Tab.Screen   name = "Poster" component ={PosterStackScreen} />
      <Tab.Screen   name = "Refund" component ={FeedbackStackScreen} />
      <Tab.Screen name="FeedbackLooping" component={LoopingStackScreen} />
     {/*  <Tab.Screen name = "Screen" component ={scre} /> */}
     

    </Tab.Navigator>
    
    );
}

export default HomeNavigator;