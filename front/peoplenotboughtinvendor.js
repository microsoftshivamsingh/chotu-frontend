import axios from 'axios';
import React,{useState,useEffect, useContext} from 'react';
import PTRView from 'react-native-pull-to-refresh';
import AppContext from '../../AppContext';
//import { SafeAreaView, StyleSheet, TextInput } from "react-native";
import { Card, Left,CardItem,Icon,Right, Body } from 'native-base';
import { Authheader } from '../../keys';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    Image,
    Button,
    Text,
    Linking,
    TextInput,
    View,
  } from 'react-native'; 


  const Pinnotboughtinvendor=({navigation,route})=>{
    const myContext=useContext(AppContext);
    const {pin,vendorid}=route.params
    const [customer_list,set_customer_list]=useState([]);
    const arr=[];
    
    const [bought,set_bought]=useState([]);
    let userMap=new Map();

    function logMapElements(value, key, map){

        arr.push({customer_id:key,type:value.type});
        console.log("logging map",arr);
        console.log(`m[${key}]=${value.type}`)
         
     }

     const fetch_vendor_details=async ()=>{

        await axios.get(`http://35.154.70.53:5000/customer/fetch?community_id=${myContext.community}`,Authheader)
        .then(res =>{
            //console.log("Axios response received data: ",res.data);
           
            console.log("backend req completed");
            res.data.map((x)=>{
                userMap.set(x.customer_id,{type:"notfromvendor"});
            })
        }).catch(err => {
            if(err.response)
            {   if (error.response.data) {
                console.log(error.response.data.errors);
                error.response.data.errors.map((e) => {
                   
                   console.log("debug e is ", e);
                   return e;
                }); }
                else{
                    console.log("Network error");
                }
            }
            else if (err.request){
                console.log("2 error->", err.message);
            }
            else {
                // Something happened in setting up the request and triggered an Error
                console.log('e Error is ', err.message);
              }
            
        })

        await axios.get(`http://35.154.70.53:5000/vendor/fetchonevendor?community_id=${myContext.community}&vendor_id=${vendorid}`,Authheader)
        .then(res =>{
            
            //console.log("Axios response received data: ",res.data);
            
            console.log("backend req completed from pin");
            console.log(res.data.customers_list);
           
            
            res.data.customers_bought.map((x)=>{
                console.log(x);
                userMap.set(x,{type:"invendor"});
            }) 

        }).catch(err => {
            if(err.response)
            {   if (err.response.data) {
                console.log(err.response.data.errors);
                err.response.data.errors.map((e) => {
                   
                   console.log("debug e is ", e);
                   return e;
                }); }
                else{
                    console.log("Network error");
                }
            }
            else if (err.request){
                console.log("2 error->", err.message);
            }
            else {
                // Something happened in setting up the request and triggered an Error
                console.log('e Error is ', err.message);
              }
            
        })


        

        userMap.forEach(logMapElements);
        set_customer_list(arr);
     }



     useEffect( () => {
        const unsubscribe = navigation.addListener('focus', () => {
             fetch_vendor_details();
          });
          return unsubscribe;
        }, [navigation]);
    

    const _refresh =  ()=> {
        return new Promise((resolve) => {
           fetch_vendor_details()
           .then(r => resolve())
          
        });
      }

      return (
        <PTRView onRefresh={_refresh} style={{backgroundColor:"#F2F8FF",height:hp('100%'),}}>
        {/* marginLeft:12,marginTop:20,marginRight:12 */}
        <ScrollView style={{marginLeft:wp("4%"),marginTop:hp("6%"),marginRight:wp("3%")}}>
        {/* <View style={{flexDirection:"row",justifyContent:"center"}}>
        <View style={{ width:130}}>
    
        </View>
        </View> */}
              <View>
                  <View style={styles4.inputLogin}>
                <View
                // style={{borderColor:"#F0F0F0"}}
                activeOpacity={0.7}
                >
                  
                  <View style={{flexDirection:"row"}}>
                  <TextInput
                   placeholder="Search Contact"
             
                   placeholderTextColor='#000000'
                //    value={search_phrase}
                //    onChangeText={e=>set_search_phrase(e)}
                  
                  style={{flex:1,backgroundColor:'#FFFFFF',borderRadius:8,borderWidth:1,fontFamily:"Muli_600SemiBold",fontSize:15,borderColor:"#F0F0F0",color:"#000000",paddingLeft:20}}
                  borderColor="#F0F0F0"
                  editable={true}>

                  </TextInput >
                  <TouchableOpacity  style={{marginTop:12,right:20,position:'absolute'}}>
                  <Image  source={require('../assets/images/ic_search_normal.png')}/>
                  </TouchableOpacity>

                  
                  </View>
                </View>
            </View>

                <View style={{top:hp("1%"),}}>
                    <Image  style={{width:wp("100%"),height:hp("15%"),position:'absolute', }} source={require('../assets/images/pin_img.png')}/>
                     <Text style={{fontSize:24,fontWeight:'bold', color:"#0E4C75", top:hp("19%")}}>Share This Offer...</Text>
                 </View>
            {
                customer_list.map((customer,indx)=>{
                    {/* console.log(customer) */}
                    if(customer.type!='invendor'){
                    return(
                        <TouchableOpacity key={indx} style={styles5.container} >
                         <Image  source={require('../assets/images/feed_img.png')}/>
                        <View style={{bottom:hp("0.3%")}}>
                        
                            <Text style={{fontSize:24,fontWeight:'bold', color:"#0E4C75"}}>Name</Text>
                      
                          <Text style={{fontSize:11,color:'#757575'}}>{customer.customer_id}</Text>
                          
                        </View>
                  
                        <TouchableOpacity style={{position:'relative',left:wp("35%"),}} onPress={()=>{Linking.openURL("https://wa.me/91"+customer.customer_id+"?text=OFFER%0a%0aHello "+"%0aVendor is back with great offers, go and checkout.%0a%0aFrom Chotu")}}>
                        <Image style={{width:wp("10%"),height:hp("5.5%"),resizeMode: "contain",}} source={require('../assets/images/whatsapp.png')}/>
                        </TouchableOpacity>
                      
                        {/* <View style={{marginLeft:20,width:200}}>
                        <Text style={{fontSize:15,fontWeight:'bold',color:"#39a8ad"}}>Name</Text>
                        <Text style={{fontSize:15,fontWeight:'bold',color:'#676b6b'}}>{customer.customer_id}</Text>
                        
                        </View> */}
                  
                        
                    </TouchableOpacity>
                    
                    )}
                })
            }
            
      </View>
        </ScrollView>
    </PTRView>

      )




}

export default Pinnotboughtinvendor;
const styles4=StyleSheet.create({
    // container: {
      
      
    //   paddingHorizontal: 24,
    //   paddingBottom:  18,
      
    // },
    // logoApp: {
    //   marginTop: 50,
    //   alignSelf: "center",
    //   alignItems: "center",
    // },
    // logo: {
    //   height:100,
    //   width:100,
    //   marginBottom: 12,
      
    // },
    inputLogin:{
      // marginTop:50,
      
      borderColor: "#728FCE",
      marginTop:5,
      marginBottom:30,
      borderRadius:10,
      // height: 40, 
      // width: "95%", 
      // borderColor: 'black', 
      borderWidth: 1,  
      marginBottom: 20
    }

  })
const styles5=StyleSheet.create({
    container:{
        padding:20,
    backgroundColor:'#FFFFFF',
    borderRadius:8,
    marginTop:8,
    marginLeft:2,
    marginRight:2,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "blue",
    shadowRadius: 4,
    elevation: 6,
    marginBottom:16,
    top:hp("20%")
    //   padding:20,
    //   backgroundColor:'#FFFFFF',
    //   borderRadius:16,
      
    //   flexDirection: "row",
    //   alignItems: "center",
    //   shadowColor: "#000",
    //   shadowOffset: {
    //     width: 0,
    //     height: 4,
    //   },
    //   shadowOpacity: 0.05,
    //   shadowRadius: 4,
    //   elevation: 4,
      
    //   marginBottom:16,
    }
  })

