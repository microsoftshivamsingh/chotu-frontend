import axios from 'axios';
import React,{useState,useEffect, useContext} from 'react';
import PTRView from 'react-native-pull-to-refresh';
import AppContext from '../../AppContext';
import { Card, Left,CardItem,Icon,Right, Body } from 'native-base';
import { Authheader } from '../../keys';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Button,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    Image
  } from 'react-native'; 

  const ListVendors=({navigation})=>{
    const myContext=useContext(AppContext);
    const [vendor_list,set_vendor_list]=useState([]);
    const fetch_vendor_details = async () => {
       
        axios.get(`http://35.154.70.53:5000/vendor/fetch?community_id=${myContext.community}`,Authheader)
        .then(res =>{
            
            //console.log("Axios response received data: ",res.data);
            set_vendor_list([...res.data]);
            console.log("backend req completed from pin");

        }).catch(err => {
            if(err.response)
            {   if (err.response.data) {
                console.log(err.response.data.errors);
                err.response.data.errors.map((e) => {
                   
                   console.log("debug e is ", e);
                   return e;
                }); }
                else{
                    console.log("Network error");
                }
            }
            else if (err.request){
                console.log("2 error->", err.message);
            }
            else {
                // Something happened in setting up the request and triggered an Error
                console.log('e Error is ', err.message);
              }
            
        })
    }

    useEffect( () => {
        const unsubscribe = navigation.addListener('focus', () => {
             fetch_vendor_details();
          });
          return unsubscribe;
        }, [navigation]);
    

    const _refresh =  ()=> {
        return new Promise((resolve) => {
           fetch_vendor_details()
           .then(r => resolve())
          
        });
      }

      return(
        <PTRView onRefresh={_refresh} style={{backgroundColor:"#F2F8FF",height:hp('100%')}}>
            
           
            <View >
          
            <View  style={{marginLeft:wp("4%"),marginTop:hp("6%"),marginRight:wp("3%"),}}>
                <View style={styles4.inputLogin}>
                <View
                // style={{borderColor:"#F0F0F0"}}
                activeOpacity={0.7}
                >
                  
                  <View style={{flexDirection:"row"}}>
                  <TextInput
                   placeholder="Search Contact"
             
                   placeholderTextColor='#000000'
                //    value={search_phrase}
                //    onChangeText={e=>set_search_phrase(e)}
                  
                  style={{flex:1,backgroundColor:'#FFFFFF',borderRadius:8,borderWidth:1,fontFamily:"Muli_600SemiBold",fontSize:15,borderColor:"#F0F0F0",color:"#000000",paddingLeft:20}}
                  borderColor="#F0F0F0"
                  editable={true}>

                  </TextInput >
                  <TouchableOpacity  style={{marginTop:12,right:20,position:'absolute'}}>
                  <Image  source={require('../assets/images/ic_search_normal.png')}/>
                  </TouchableOpacity>

                  
                  </View>
                </View>
            </View>
            <ScrollView style={{backgroundColor:"#F2F8FF",height:hp('100%')}}>
                {
                    vendor_list.map((vendor,indx)=>{
                        return(
                            <TouchableOpacity key={indx} style={styles5.container} onPress={()=>{navigation.navigate('PINs',{vendorid:vendor.id})}}>

                            <Image source={require('../assets/images/feed_img.png')}/>
                            <View style={{bottom:hp("0.3%")}}>
                            <Text style={{fontSize:24,fontWeight:'bold', color:"#0E4C75"}}>{vendor.name}</Text>
                            <Text style={{fontSize:11,color:'#757575'}}>ID:{vendor.id}</Text>
                            {/* <View style={{flexDirection:'row'}}>
                            </View> */}
                            </View>
                            
                            <Image style={{width:wp("5%"),height:hp("1.7%"),position:'absolute',right:wp("6%")}} source={require('../assets/images/singlesmall.png')}/>
                            
                            </TouchableOpacity>

                        );
                    })
                }
                </ScrollView>
           </View>
           </View>
      
            {/* </ScrollView> */}
        </PTRView>
    )
  }



  const styles = StyleSheet.create({
    header:{
        height:20,
        margin:5

    },
    input: {
        height: 40,
        marginTop:3,
        marginBottom:7,
        marginLeft:7,
        marginRight:7,
        borderWidth: 1,
      },

})
const styles4=StyleSheet.create({
    // container: {
      
      
    //   paddingHorizontal: 24,
    //   paddingBottom:  18,
      
    // },
    // logoApp: {
    //   marginTop: 50,
    //   alignSelf: "center",
    //   alignItems: "center",
    // },
    // logo: {
    //   height:100,
    //   width:100,
    //   marginBottom: 12,
      
    // },
    inputLogin:{
      // marginTop:50,
      
      borderColor: "#728FCE",
      marginTop:5,
      marginBottom:30,
      borderRadius:10,
      // height: 40, 
      // width: "95%", 
      // borderColor: 'black', 
      borderWidth: 1,  
      marginBottom: 20
    }

  })

const styles5=StyleSheet.create({
    container:{
      padding:20,
    backgroundColor:'#FFFFFF',
    borderRadius:10,
    marginTop:8,
    marginLeft:2,
    marginRight:2,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "blue",
    shadowRadius: 4,
    elevation: 6,
    marginBottom:16,

    }
  })


  export default ListVendors;
