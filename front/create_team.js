import * as React from 'react';
import { useState ,useEffect} from 'react';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { NavigationContainer } from '@react-navigation/native';
import { ActivityIndicator, Alert } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import AppContext from '../../AppContext';
import RadioButtonRN from 'radio-buttons-react-native';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TouchableOpacity,
  CheckBox,
  TextInput,
  Image,
  Icon,
  Button,
  View,
  ImageBackground
} from 'react-native';
import { Picker } from '@react-native-community/picker'
import { color } from 'react-native-reanimated';
import ImagePicker from 'react-native-image-crop-picker';
import axios from 'axios';
import { Authheader } from '../../keys';
import DropDownPicker from 'react-native-dropdown-picker';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';



const Create_team = ({navigation}) => {
  const myContext = React.useContext(AppContext)
  const [gatedLocality, setGated] = useState(null)
  const [pickupAddress, setPickup] = useState("")
  const [address, setAddress] = useState("")
  const [team_name, setTeam_name] = useState("")
  const [team_id, setTeam_id] = useState("")
  const [full_gated,set_full_gated]=useState({})
  const [team_url, setTeam_url] = useState("")
  const [team_urlalias, setTeam_urlalias] = useState("")
  const [team_description, setTeam_description] = useState("")
  const [team_link_whatsapp, setTeam_link_whatsapp] = useState("")
  const [team_link_telegram, setTeam_link_telegram] = useState("")
  const [team_social_open, setTeam_social_open] = useState("")
  const [team_update_date, setTeam_update_date] = useState("")
  const [team_update_user_ID, setTeam_update_user_ID] = useState("")
  const [filePath, setFilePath] = useState("");
  const [uris,set_uris]=useState("");
  const [status,setStatus] = useState("Pending");
  const [show,set_show]=useState(false);
  const [open1,set_open1]=useState(false);
  const [items, setItems] = useState([
     
  ]);
  


  const getGatedlocalities= async()=>{
    console.log("came to get gated localities")
    await axios.get("https://chotu.shop/wp-json/v2/get_gated_locality")
    .then(res=>{
      console.log("came to res data"+res.data)
      let temp=[]
      let temp2={}
      res.data.forEach(e=>{
        temp.push({label:e.post_title,value:e.ID})
        temp2[e.ID]=e
      })
      setItems([...temp])
      set_full_gated({...temp2})
      console.log(temp)
    })
    .catch(err=>{
      console.log(err)
    })
  }

  const submitfn = async () => {
    //await myContext.setcomname(team_id);
      const authdetails = {
        team_name:team_name,
        gated_locality_name:full_gated[gatedLocality].post_title,
        gated_locality_id:full_gated[gatedLocality].ID,
        gated_locality_postal_address:full_gated[gatedLocality].meta.full_postal_address[0],
        gated_locality_city:full_gated[gatedLocality].meta.gated_locality_city[0],
        gated_locality_pin_code:full_gated[gatedLocality].meta.gated_locality_pin_code[0],
        address:address,
        pickup_address:pickupAddress,
        captain_id:myContext.captain_id,
        image:filePath,
        link_to_whatsapp:team_link_whatsapp,
        social_open:team_social_open,
        link_to_telegram:team_link_telegram,
        team_description:team_description,
        team_status:"pending"

      }
      console.log(authdetails)
      await axios.post("http://65.1.88.93:5000/team/add",authdetails,Authheader)
      .then((res)=>{
        if(res.data.msg == "Request sent to create a team")
        {
            console.log(res.data.msg)
            myContext.set_team_created(true)
            alert(res.data.msg)
        }
        else if(res.data.msg=="A captain can have atmost one team"){
          console.log(res.data.msg)
          alert(res.data.msg)
        }
        
      })
      .catch(err=>{
        console.log(err)
      })
  }

  const chooseFile=()=>{
    ImagePicker.openPicker({
      width: 400,
      height: 200,
      cropping: true,
      includeBase64:true
    }).then(image => {
      set_uris(image.path)
      setFilePath(image.data)
      
    })
    .catch(err=>{
      console.log(err)
    }
    )
  }
  

  const storeData = async () => {
    try {
      await AsyncStorage.removeItem('captain_id')
      await AsyncStorage.removeItem('captain_mobile')
      await AsyncStorage.removeItem('team_id')
      console.log("removed data")
    } catch (e) {
      console.log('came here')
    }
  }

  const logoutfn=async ()=>{
    await storeData();
    myContext.setloginvalue(false)
  }


    // radio button
    var radio_props = [
  {label: 'Yes', value: 0 },
  {label: 'No', value: 1 }
];
//   const [isLiked, setIsLiked] = useState([
//     { id: 1, value: true, name: "Yes", selected: false },
//     { id: 2, value: false, name: "No", selected: false }
//   ]);
//   const RadioButton = ({ onPress, selected, children }) => {
//   return (
//     <View style={styles.radioButtonContainer}>
//       <TouchableOpacity onPress={onPress} style={styles.radioButton}>
//         <View style={{}}>
//         {/* {selected ? <View style={styles.radioButtonIcon} /> : null} */}
//         </View>
//       </TouchableOpacity>
      
//       <TouchableOpacity onPress={onPress}>
//         {/* <Text style={styles.radioButtonText}>{children}</Text> */}
//       </TouchableOpacity>
//     </View>
//   );
// };
// const onRadioBtnClick = (item) => {
//     let updatedState = isLiked.map((isLikedItem) =>
//       isLikedItem.id === item.id
//         ? { ...isLikedItem, selected: true }
//         : { ...isLikedItem, selected: false }
//     );
//     setIsLiked(updatedState);
//   };

  useEffect(()=>{
    getGatedlocalities()
  },[])
  
  return (
    <ScrollView>
    <View>
      <View style={{ backgroundColor: "#F2F8FF", height: hp("160%") }}>
       <TouchableOpacity style={{position:'absolute', right:20}} onPress={()=>logoutfn()}>
          <Image style={{height:35,width:35}} source={require('../assets/images/logout.png')}/>
        </TouchableOpacity>
        <View style={styles4.container}>
       

           <View style={{top:hp("3%")}}>
               <TouchableOpacity 
                  onPress={()=>chooseFile()}
                  style={{alignItems:'center'}}>
                  <ImageBackground style={{height:hp("15%"),width:wp("100%"),}} source={require('../assets/images/banner_img.png')}>

                    <Image style={{left:wp("37%"),marginTop:hp("4%")}} source={require('../assets/images/banner_img1.png')} />
                  </ImageBackground>
                 
               </TouchableOpacity>
                {
              uris!=""?<View style={{alignItems:'center',bottom:hp("15%")}}>

                <Image
                source={{uri:uris}}
                style={{height:hp("15%"),width:wp("100%")}}
                >
                </Image>
               

                </View>:null
            }
            </View>



        <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13, color: "#0f4c75", fontWeight: "bold",  }}>
                Enter team name
              </Text>
              <TextInput
                value={team_name}
                // placeholder = "enter team name"
                // placeholderTextColor = "grey"
               
                onChangeText={e => setTeam_name(e)}
                style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}
                editable={true}

              >
              </TextInput>
            </TouchableOpacity>
          </View>
          <View style={{marginTop:35}}>
            {/* <Text style={{fontSize:15,marginBottom:2}}>Gated Locality</Text> */}
            <DropDownPicker
                
                open={open1}
                value={gatedLocality}
                placeholder="select gated locality"
                placeholderTextColor = "grey"
                items={items}
                searchable={true}
                style={{height:50}}
                listMode="SCROLLVIEW"
                setOpen={set_open1}
                setValue={setGated}
                setItems={setItems}
                    
            />
            </View>
            {
              gatedLocality?
              <View>
                <View style={styles4.inputLogin}>
                  <TouchableOpacity
                    style={{ borderColor: "#F0F0F0" }}
                    activeOpacity={0.7}
                  >
                    <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                     GL Name
                    </Text>
                    <TextInput
                      value={full_gated[gatedLocality].post_title}
                      onChangeText={e => setPickup(e)}
                      style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}

                     

                    >

                    </TextInput>
                  </TouchableOpacity>
                </View>
                <View style={styles4.inputLogin}>
                  <TouchableOpacity
                    style={{ borderColor: "#F0F0F0" }}
                    activeOpacity={0.7}
                  >
                    <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                      GL Address
                    </Text>
                    <TextInput
                      value={full_gated[gatedLocality].meta["full_postal_address"][0]}
                      
                      style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}

                      

                    >

                    </TextInput>
                  </TouchableOpacity>
                </View>
                <View style={styles4.inputLogin}>
                  <TouchableOpacity
                    style={{ borderColor: "#F0F0F0" }}
                    activeOpacity={0.7}
                  >
                    <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                      GL City
                    </Text>
                    <TextInput
                      value={full_gated[gatedLocality].meta["gated_locality_city"][0]}
                      
                      style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}

                     

                    >

                    </TextInput>
                  </TouchableOpacity>
                </View>
                <View style={styles4.inputLogin}>
                  <TouchableOpacity
                    style={{ borderColor: "#F0F0F0" }}
                    activeOpacity={0.7}
                  >
                    <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                      GL Pincode
                    </Text>
                    <TextInput
                      value={full_gated[gatedLocality].meta["gated_locality_pin_code"][0]}
                      
                      style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}

                      

                    >

                    </TextInput>
                  </TouchableOpacity>
                </View> 
              </View>:null
            }
          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13,  fontWeight: "bold", color: '#0f4c75' }}>
                Enter pickup address
              </Text>
              <TextInput
                value={pickupAddress}
                // placeholder="Enter pickup address"
                // placeholderTextColor = "grey"
                 multiline = {true}
                numberOfLines= {10}
                onChangeText={e => setPickup(e)}
                style={{ height: hp("10%"),backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}
                textAlignVertical = {"top"}
                editable={true}

              >

              </TextInput>
            </TouchableOpacity>
          </View> 
          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13,  fontWeight: "bold", color: '#0f4c75' }}>
               Enter captain address
              </Text>
              <TextInput
                value={address}
                // placeholder="Enter captain address"
                // placeholderTextColor = "grey"
                 multiline = {true}
                 textAlignVertical= {"top"}
                numberOfLines= {10}
                onChangeText={e => setAddress(e)}
                style={{ height: hp("10%"),backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000' }}

                editable={true}

              >

              </TextInput>
            </TouchableOpacity>
          </View>
          {/* <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                Team url
              </Text>
                <TextInput
                  value={team_url}
                  onChangeText={e => setTeam_url(e)}
                  style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000', width: 300 }}
                >
                </TextInput>
            </TouchableOpacity>
          </View>
          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                Team url alias
              </Text>
                <TextInput
                  value={team_urlalias}
                  onChangeText={e => setTeam_urlalias(e)}
                  style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000', width: 300 }}
                >
                </TextInput>
            </TouchableOpacity>
          </View> */}
          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13, fontWeight: "bold", color: '#0f4c75' }}>
                Team whatsapp link
              </Text>
                <TextInput
                  value={team_link_whatsapp}
                  //  placeholder="Team whatsapp link"
                  //   placeholderTextColor = "grey"
                  onChangeText={e => setTeam_link_whatsapp(e)}
                  style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000',  }}
                >
                </TextInput>
            </TouchableOpacity>
          </View>
          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13,  fontWeight: "bold", color: '#0f4c75' }}>
                Team telegram link
              </Text>
                <TextInput
                  value={team_link_telegram}
                  // placeholder="team telegram link(optional)"
                  //   placeholderTextColor = "grey"
                  onChangeText={e => setTeam_link_telegram(e)}
                  style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000',  }}
                >
                </TextInput>
            </TouchableOpacity>
          </View>


          <View>

                <Text style={{left:hp("2%"), marginTop: wp("4%"), fontSize: 15, color: '#0E4C75',fontWeight:"bold"  }}>Can anyone join?</Text>
                {/* {isLiked.map((item) => (
     <RadioButton
       onPress={() => onRadioBtnClick(item)}
       selected={item.selected}
       key={item.id}
     >
       {item.name}
     </RadioButton>
  ))} */}
              
              <RadioForm
              style={{left:hp("1.7%"), top:wp("2%"),}}
                radio_props={radio_props}
                initial={0}
                onPress = {(value) =>{}}
                formHorizontal={true}
                buttonSize={13}
                buttonOuterSize={25}
                labelColor={"#0E4C75"}
                labelStyle={{fontWeight: "bold",marginRight:wp("10%")}}
                animation={true}
                buttonColor={'#0E4C75'}
                
                
                />

           
              
          </View>
          {/* <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                Team social open
              </Text>
                <TextInput
                  value={team_social_open}
                  onChangeText={e => setTeam_social_open(e)}
                  style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000', width: 300 }}
                >
                </TextInput>
            </TouchableOpacity>
          </View> */}
          {/* <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                Team update date
              </Text>
                <TextInput
                  value={team_update_date}
                  onChangeText={e => setTeam_update_date(e)}
                  style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000', width: 300 }}
                >
                </TextInput>
            </TouchableOpacity>
          </View>
          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                Team update user id
              </Text>
                <TextInput
                  value={team_update_user_ID}
                  onChangeText={e => setTeam_update_user_ID(e)}
                  style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000', width: 300 }}
                >
                </TextInput>
            </TouchableOpacity>
          </View> */}
          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13,  fontWeight: "bold", color: '#0E4C75' }}>
                Enter description
              </Text>
                <TextInput
                  // value={team_description}
                  //  placeholder="Enter description"
                   multiline={true}
                   numberOfLines= {10}
                   textAlignVertical = {"top"}
                    placeholderTextColor = "grey"
                  onChangeText={e => setTeam_description(e)}
                  style={{ height:hp("15%"),backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000',}}
                >
                </TextInput>
            </TouchableOpacity>
          </View>

            {/* <View style={{top:hp("2%")}}>
               <TouchableOpacity 
                  onPress={()=>chooseFile()}
                  style={{marginTop:10,backgroundColor:"#0f4c75",padding:15,borderRadius:12}}>
                  <Text style={{fontWeight:"bold",color:"#FFFFFF",textAlign:"center",fontSize:15}}>UploadImage</Text>
               </TouchableOpacity>
            </View>

            {
              uris!=""?<View style={{alignItems:'center'}}>

                <Image
                source={{uri:uris}}
                style={{height:100,width:200}}
                >
                </Image>
               

                </View>:null
            } */}

          <TouchableOpacity
            onPress={() => submitfn()}
            style={{ marginTop: 40, backgroundColor: "#0f4c75", padding: 15, borderRadius: 12 }}>
            <Text style={{ fontWeight: "bold", color: "#FFFFFF", textAlign: "center", fontSize: 15 }}>Submit</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
    </ScrollView>
  )

}
export default Create_team;

const styles4 = StyleSheet.create({
  container: {

    top:hp("8%"),
    paddingHorizontal: 24,
    paddingBottom: 18,

  },
  logoApp: {
    marginTop: 50,
    alignSelf: "center",
    alignItems: "center",
  },
  logo: {
    height: 100,
    width: 100,
    marginBottom: 12,

  },
  inputLogin: {
    marginTop: 50,
  },
  

})

// const styles = StyleSheet.create({

//   radioButtonContainer: {
//     flexDirection: "row",
//     alignItems: "center",
//     left: wp("5%"),
//     marginTop:10,
//     // flex: 1,
    
 

//   },
//   radioButton: {
//     flexDirection: "row",
//     height: 22,
//     width: 22,
//     backgroundColor: "#F8F8F8",
//     borderRadius: 12,
//     borderWidth: 1,
//     borderColor: "#0E4C75",
//     alignItems: "center",
//     // justifyContent: "center",
   
//   },
//   radioButtonIcon: {
//     height: 12.5,
//     width: 12.5,
//     borderRadius: 7,
//     backgroundColor: "#0E4C75",
//     // flexDirection: "row",
//   },
//   radioButtonText: {
//     fontSize: 16,
//     marginLeft: 16,
//     // flexDirection: "row",
//   }

// })
