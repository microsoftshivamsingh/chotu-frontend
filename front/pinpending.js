import axios from 'axios';
import React, { useState, useEffect, useContext } from 'react';



import { Container, Header, Content, Card, Left, CardItem, Icon, Right, Body, Title } from 'native-base';
import { Authheader } from '../../keys';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TouchableOpacity,
  TextInput,
  Image,
  Button,
  View,
} from 'react-native';
import AppContext from '../../AppContext';


const Pinpending = ({ navigation, route }) => {
  const { pin } = route.params
  const [orders, set_orders] = useState([]);
  const myContext = useContext(AppContext)

  const fetch_order_details = async () => {
    axios.get(`http://35.154.70.53:5000/order/pin_fetch?pin=${pin}&community_id=${myContext.community}`, Authheader)
      .then(res => {
        /* set_transactions([]) */
        const temp = []
        res.data.forEach(a => {
          //console.log(a)
          if (a.invoice == null) {
            temp.push(a)
          }
        })

        temp.forEach(or => { or["type"] = "order", or["date"] = or.date_of_order })


        set_orders([...orders, ...temp])
        console.log("from order", temp.length)

      })

  }


  const compare = (a, b) => {
    const ad = new Date(a.date_of_order)
    const bd = new Date(b.date_of_order)
    if (ad < bd)
      return 1;
    else
      return -1;

  }
  var weekday = new Array(7);
weekday[0] = "Sun";
weekday[1] = "Mon";
weekday[2] = "Tue";
weekday[3] = "Wed";
weekday[4] = "Thu";
weekday[5] = "Fri";
weekday[6] = "Sat";

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      fetch_order_details();
    });
    return unsubscribe;
  }, [navigation]);

  return (
 
    <View style={{backgroundColor:"#F2F8FF",height:hp("100%"),}}>
    
        <View style={{backgroundColor:"#0E4C75",flexDirection:'row',height:hp("16%"),width:wp("100%"),}}>
            <View style={{top:hp("7%"),left:wp("4%"),position:'absolute'}}>
            <Text style={{fontSize:25,fontWeight:'bold',color:'#FFFFFF'}}>{pin}</Text>
            <Text style={{fontSize:15,color:'#FFFFFF'}}>02 Jul, Thu</Text>
            </View>
        </View>
         {/* <View style={{bottom:hp("13%"),}}>
          
           <TouchableOpacity  style={styles5.container1} button >
                    
                    <View >
                      <View style={{bottom:hp("0.3%"),}}>
                      <Text style={{fontSize:22,fontWeight:'bold', color:"#0E4C75"}}>{pin} </Text>
                      <Text style={{fontSize:11,fontWeight:'bold',color:'#757575'}}>{pin} </Text>
                      </View>
                    </View>
                    <Image style={{width:wp("5%"),height:hp("1.7%"),position:'absolute',right:wp("6%")}} source={require('../assets/images/singlesmall.png')}/>
                 </TouchableOpacity>
           </View>
      <View style={{ flexDirection: "row", marginTop: 15 ,marginBottom:5,marginLeft:20}} >

        <Text style={{ fontSize: 20,textTransform:'uppercase',color:'#0f4c75'}}> Pin:</Text>
        <Text style={{fontSize: 20,color:'black',color:'#0f4c75' }}> {" "}{pin}</Text>
      </View> */}
      <ScrollView >
      {orders.sort(compare).map((a, indx) => {
        console.log(a)
        let tempdate=new Date(a.date_of_order).getDay();
        let day=weekday[tempdate];
        let finaldate=a.date_of_order.substr(0, 10)+" ("+day+")";
        return (
     
    
          <View key={indx} style={{backgroundColor:"#F2F8FF",height:hp("100%"),}}>
          <View >
            <TouchableOpacity style = {styles5.container1} onPress={() => { navigation.navigate('editpending', { customer_mobi: a.customer_id, customer_nam: a.customer_name, order_id: a._id, initial: a.total, type: a.payment_method, oid: a.id, date2: a.date_of_order, pin_code: pin }) }}>
                {/* <Image style={{width:40,height:40}} source={require('../assets/images/bag.png')}/> */}
                <View style={{width:wp("100%"),}}>
                <Text style ={{fontSize:22,fontWeight:'bold', color:"#0E4C75",width:wp("70%"),}}>Ceicillia Chapman </Text>
                <Text style ={{fontSize:11, color:"#676b6b"}}>{a.customer_name}</Text>
                  {/* <Text style ={{fontSize:13,fontWeight:'bold',color:'#676b6b',marginTop:1}}>ID:{a.id}</Text> */}
                  <Text style ={{fontSize:13,color:'#676b6b',marginTop:1}}>{a.customer_id}</Text>
                  {/* <Text style={{fontSize:13,fontWeight:'bold',color:'#676b6b',marginTop:1}}>{a.date_of_order.substr(0, 10)}</Text> */}
                </View>
                {/* <View>
                  <Text style={{fontSize:13,fontWeight:'bold',color:'#39a8ad'}}>Amount:{a.total}</Text>
                  <Text></Text>
                  <Text></Text>
                  <Text style={{fontSize:13,fontWeight:'bold',color:'#39a8ad'}}>Mode:{a.payment_method}</Text>
                </View> */}
                {/* style={{marginTop:5,marginBottom:5,width:15,height:15,left:15}} */}
                <Image style={{width:wp("5%"),height:hp("1.7%"),position:'absolute',right:wp("6%")}} source={require('../assets/images/singlesmall.png')}/>

            </TouchableOpacity>

            {/* <Card >
            <CardItem button onPress={() => { navigation.navigate('editpending', { customer_mobi: a.customer_id, customer_nam: a.customer_name, order_id: a._id, initial: a.total, type: a.payment_method, oid: a.id, date2: a.date_of_order, pin_code: pin }) }}>
              <Body>
                <Left>
                  <View>
                    <View style={{flexDirection:'row'}}>
                    <Text style={{flex:1}}> Name:{a.customer_name}</Text>
                    <Text style={{flex:1}}>OrderID :{a.id} </Text>
                    </View>
                  </View>
                  
                  <Text>Phone :{a.customer_id}</Text>
                  <Text >Date:{a.date_of_order.substr(0, 10)}</Text>
                  <Text style={{ color: '#0f4c75', fontWeight: "bold" }}>Amount:{a.total}</Text>
                  <Text style={{ color: '#0f4c75', fontWeight: "bold" }}>payment method :{a.payment_method}</Text>
                </Left>
              </Body>
              <Right>
                <Icon style={{ color: "green" }} name='arrow-forward' />
              </Right>
            </CardItem>
          </Card> */}
          </View>
          </View>
       
        )
      })}
         </ScrollView>
   
  </View>
 

  )
}

const styles5=StyleSheet.create({
  container:{
    padding:40,
    backgroundColor:'#FFFFFF',

    borderRadius:10,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "blue",
    
    top:100,
    shadowRadius: 4,
    elevation: 6,
    marginBottom:16,
  },
  container1:{
   padding:40,
    backgroundColor:'#FFFFFF',
    borderRadius:10,
    top:hp("2%"),
    marginTop:hp("1.5%"),
    marginLeft:wp("4%"),
    marginRight:wp("4%"),
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "blue",
    shadowRadius: 4,
    elevation: 6,
    marginBottom:16,
  },

})

export default Pinpending;
