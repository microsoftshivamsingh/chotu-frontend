import axios from 'axios';
import React,{useState,useEffect, useContext} from 'react';
import PTRView from 'react-native-pull-to-refresh';
import AppContext from '../../AppContext';
//import { SafeAreaView, StyleSheet, TextInput } from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { Authheader } from '../../keys';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Button,
    Text,
    TextInput,
    View,
    Linking,
    TouchableOpacity,
    Image
  } from 'react-native'; 

  export const Feedbackmain = ({navigation}) => {
    const myContext=useContext(AppContext);
    const [customers_list,set_customers_list]=useState([]);
    const [search_phrase,set_search_phrase] = useState("");

    const fetch_customer_details = async () => {
        console.log("sending backened req");
        axios.get(`http://35.154.70.53:5000/customer/fetch?community_id=${myContext.community}`,Authheader)
        .then(res =>{
            //console.log("Axios response received data: ",res.data);
            set_customers_list([...res.data]);
            console.log("backend req completed");

        }).catch(err => {
            if(err.response)
            {   if (error.response.data) {
                console.log(error.response.data.errors);
                error.response.data.errors.map((e) => {
                   
                   console.log("debug e is ", e);
                   return e;
                }); }
                else{
                    console.log("Network error");
                }
            }
            else if (err.request){
                console.log("2 error->", err.message);
            }
            else {
                // Something happened in setting up the request and triggered an Error
                console.log('e Error is ', err.message);
              }
            
        })
    }

    useEffect( () => {
        const unsubscribe = navigation.addListener('focus', () => {
            fetch_customer_details();
          });
          return unsubscribe;
        }, [navigation]);
    
        /* onPress={()=>{Linking.openURL("https://wa.me/91"+customer.mobile+"?text=reminder%0a%0aHello "+customer.name+" garu.%0aBalance pending to be paid is "+customer.balance*-1+".%0aThank you.%0aFrom Chotu")}} */
        /* button onPress={() => {navigation.navigate('Ledger',{customer_mob:customer.mobile,customer_name:customer.name})} } */
   const  _refresh =  ()=> {
        return new Promise((resolve) => {
            fetch_customer_details()
           .then(r => resolve())
          
        });
      }

      return(
          <PTRView onRefresh={_refresh} style={{backgroundColor:"#F2F8FF",height:'100%'}}>
       <ScrollView style={{marginLeft:wp("4%"),marginTop:hp("6%"),marginRight:wp("3%")}}>
        <View>
        
           <View style={styles4.inputLogin}>
                <View
                // style={{borderColor:"#F0F0F0"}}
                activeOpacity={0.7}
                >
                  
                  <View style={{flexDirection:"row"}}>
                  <TextInput
                   placeholder="Search Contact"
             
                   placeholderTextColor='#000000'
                   value={search_phrase}
                   onChangeText={e=>set_search_phrase(e)}
                  
                  style={{flex:1,backgroundColor:'#FFFFFF',borderRadius:8,borderWidth:1,fontFamily:"Muli_600SemiBold",fontSize:15,borderColor:"#F0F0F0",color:"#000000",paddingLeft:20}}
                  // borderColor="#F0F0F0"
                  editable={true}>

                  </TextInput >
                  <TouchableOpacity  style={{marginTop:12,right:20,position:'absolute'}}>
                  <Image  source={require('../assets/images/ic_search_normal.png')}/>
                  </TouchableOpacity>
                  </View>
                </View>
            </View>

            {customers_list.map((customer,indx)=>{
                if (customer.name.toLowerCase().includes(search_phrase.toLowerCase()) || customer.mobile.includes(search_phrase))
                {  
                    return(
            
                        
                        <TouchableOpacity key={indx} style={styles5.container} button onPress={() => {navigation.navigate('Orders',{customer_mob:customer.mobile,customer_name:customer.name})} }>
                        <Image  source={require('../assets/images/feed_img.png')}/>
                        <View style={{bottom:hp("0.3%")}}>
                        
                        <Text style={{fontSize:24,fontWeight:'bold', color:"#0E4C75"}}>{(customer.name).length < 13 ? customer.name : (customer.name).substring(0, 12)}</Text>
                      
                          <Text style={{fontSize:11,color:'#0E4C75'}}>{customer.mobile}</Text>
                        </View>
                        
                     
                        <Image style={{width:wp("5%"),height:hp("1.7%"),position:'absolute',right:wp("6%")}} source={require('../assets/images/singlesmall.png')}/>
                        </TouchableOpacity>
                   
                 
                        )
                }
            })}
        </View>
        </ScrollView>
        </PTRView>
      )


  }
export default Feedbackmain;
  const styles4=StyleSheet.create({
    container: {
      
      
      paddingHorizontal: 24,
      paddingBottom:  18,
      
    },
    logoApp: {
      marginTop: 50,
      alignSelf: "center",
      alignItems: "center",
    },
    logo: {
      height:100,
      width:100,
      marginBottom: 12,
      
    },
    inputLogin:{
       borderColor: "#728FCE",
      marginTop:5,
      marginBottom:30,
      borderRadius:10,
      // height: 40, 
      // width: "95%", 
      // borderColor: 'black', 
      borderWidth: 1,  
      marginBottom: 20
    }

  })

const styles5=StyleSheet.create({
  container:{
    padding:20,
    backgroundColor:'#FFFFFF',
    borderRadius:10,
    marginTop:8,
    marginLeft:2,
    marginRight:2,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "blue",
    shadowRadius: 4,
    elevation: 6,
    marginBottom:16,
  }
})