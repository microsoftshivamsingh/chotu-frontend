import * as React from 'react';
import { useState,useEffect,useRef } from 'react';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { Styles } from '../components/Styles';
import colors from '../components/Colors';
import firebase from "firebase/app";

import { NavigationContainer } from '@react-navigation/native';
import { ActivityIndicator, Alert } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import AppContext from '../../AppContext';
import CustomText from '../components/CustomText';
import ErrorBoundary from '../components/ErrorBoundry';
import CustomTextInput from '../components/CustomTextInput';
import FullButtonComponent from '../components/FullButtonComponent';
import auth from '@react-native-firebase/auth'
import LottieView from 'lottie-react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TouchableOpacity,
  CheckBox,
  TextInput,

  Image,
  Icon,
  Button,
  View,
} from 'react-native';

const Otp = ({ route,navigation ,props}) => {
  const [mobile,setMobile] = useState("");
  const [otp, setOtp] = useState("");
  const [confirm, setConfirm] = useState(null);
  const [showOtpbar,setshowOtpbar] = useState(false)



  const signInWithPhoneNumber = async () =>{
    try{
      /* const confirmation = await auth().signInWithPhoneNumber("+91"+mobile)
      console.log(confirmation);*/
      /* setConfirm(confirmation); */
      setshowOtpbar(true) 
      
      /* firebase.auth().onAuthStateChanged( (user) => {
        if (user) {
            // Obviously, you can add more statements here, 
            //       e.g. call an action creator if you use Redux. 

            // navigate the user away from the login screens: 
            navigation.navigate('Register',{cap_number:mobile});
        }
      }) */
    }
    catch(e){
      console.log("Error");
      console.log(e);
      alert(JSON.stringify(e));
    }

  }


  const submit = async () =>{

    /* const response = await confirm.confirm(otp);
    console.log(response);
    if(response){
      console.log("Successful");
      
    } */
    navigation.navigate('Register',{cap_number:mobile});
  }


  if(showOtpbar == true)
  return (
    <View >
    
             {/* <Text style={{ fontSize: 30, color: "black", fontWeight: "bold", color: '#000000' ,textAlign: "center"}}>
              Create Account
            </Text>
          <View style={styles4.inputLogin}>
            <View style={{flexDirection:"row"}}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                Mobile Number
              </Text>
              <View style={{ flexDirection: "row" }}>
               
                <TextInput
                  value={mobile}
                  onChangeText={e => setMobile(e)}
                  style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000', width: 250 }}
                  editable={false}
                  keyboardType="numeric"
                >
                </TextInput>
                <TouchableOpacity
            onPress={() => signInWithPhoneNumber()}
            style={{ marginTop: 40, backgroundColor: "#0f4c75", padding: 15, borderRadius: 12 }}>
            <Text style={{ fontWeight: "bold", color: "#FFFFFF", textAlign: "center", fontSize: 15 }}>Send OTP</Text>
          </TouchableOpacity>
              </View>
              
            </TouchableOpacity>
            
          </View>
          </View> */}
          
          
          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
             
                <TextInput
                placeholder="Enter OTP here"
                placeholderTextColor = "grey"
                  value={otp}
                  onChangeText={e => setOtp(e)}
                  style={{ marginHorizontal:24,backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000',paddingLeft:10,}}
                  editable={true}
                  keyboardType="numeric"
                >
                </TextInput>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => submit()}
            style={{marginTop: hp("7%"), backgroundColor: "#0f4c75", padding: 15, borderRadius: 12,marginHorizontal:24}}>
            <Text style={{ fontWeight: "bold", color: "#FFFFFF", textAlign: "center", fontSize: 15 }}>Verify</Text>
          </TouchableOpacity>
         
         
  </View>
  )
  else
return(
  <View>
   <View style={{ backgroundColor: "#F2F8FF", height: "100%" }}>
        <View style={styles4.container}>
          <View style={styles4.logoApp}>
            <Text style={{ fontSize: 30, color: "black", fontWeight: "bold", color: '#000000' }}>
              Create Account
            </Text>
          </View>

          <View style={{ height:hp("20%"),width:wp("40%"),left:wp("25%"), }}>
             <LottieView
                source={require("../assets/images/gif3.json")}
                size={68}
                autoPlay
                loop

              />
          </View>  


          <View style={styles4.inputLogin}>
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
          
              <View style={{ flexDirection: "row" }}>
            
              <TextInput
                value={mobile}
                onChangeText={e => setMobile(e)}
                placeholder="enter your number"
                placeholderTextColor = "grey"
                keyboardType="numeric"
                style={{ flex: 1,  borderRadius: 8, borderWidth: 1,padding: 1, fontFamily: "Muli_600SemiBold", fontSize: 15, borderColor: "#000000", color: '#000000', borderRightWidth:0,}}

                editable={true}

              >

              </TextInput>
       
              <TouchableOpacity
            onPress={() => signInWithPhoneNumber()}
            style={{ backgroundColor: "#0f4c75", padding: 14, height: hp("5%"),borderWidth: 1,borderLeftWidth:0, borderTopRightRadius: 8,borderBottomRightRadius: 8,borderTopRightRadius: 8,}}>
            <Text style={{ fontWeight: "bold", color: "#FFFFFF", textAlign: "center", fontSize: 12, }}>SEND OTP</Text>
          </TouchableOpacity>

              </View>
            </TouchableOpacity>
          </View>
          <View style={styles4.inputLogin, { marginTop: 20 }}>
            <View
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
        </View>
      </View>
   </View>
</View>
    

      <View style={{ height:hp("20%"),width:wp("40%"),left:wp("25%"), }}>
             <LottieView
                source={require("../assets/images/login_gif.json")}
                size={68}
                autoPlay
                loop

              />
          </View>  


  {/* <View style={styles4.inputLogin}>
            <View style={{flexDirection:"row"}}>
          
            <TouchableOpacity
              style={{ borderColor: "#F0F0F0" }}
              activeOpacity={0.7}
            >
              <Text style={{ fontSize: 13, color: "black", fontWeight: "600", color: '#000000' }}>
                Mobile Number
              </Text>
              <View style={{ flexDirection: "row" }}>
              
                <TextInput
                  value={mobile}
                  onChangeText={e => setMobile(e)}
                  style={{ backgroundColor: '#FFFFFF', borderRadius: 8, borderWidth: 1, marginTop: 4, fontSize: 15, borderColor: "#F0F0F0", color: '#000000', width: 250 }}
                  editable={true}
                  keyboardType="numeric"
                >
                </TextInput>
                <TouchableOpacity
            onPress={() => signInWithPhoneNumber()}
            style={{ marginTop: 40, backgroundColor: "#0f4c75", padding: 15, borderRadius: 12 }}>
            <Text style={{ fontWeight: "bold", color: "#FFFFFF", textAlign: "center", fontSize: 15 }}>Send OTP</Text>
          </TouchableOpacity>
              </View>
              
            </TouchableOpacity>
            
          </View>
          </View> */}
          </View>
)
}

export default Otp;
const styles = StyleSheet.create({
  container: {
    padding: 16,
    flex: 1,
    alignItems: 'center',
    paddingTop: 130,
  },
  submitButtonText: {
    color: colors.WHITE,
  },
  otpText: {
    color: colors.BLUE,
    fontSize: 18,
    width: '100%',
    textAlign:'center',
  },
});
const styles4 = StyleSheet.create({
  container: {


    paddingHorizontal: 24,
    paddingBottom: 18,

  },
  logoApp: {
    marginTop: 50,
    alignSelf: "center",
    alignItems: "center",
  },
  logo: {
    height: 100,
    width: 100,
    marginBottom: 12,

  },
  inputLogin: {
    marginTop: 50,
  }

})