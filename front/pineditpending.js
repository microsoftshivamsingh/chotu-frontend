import * as React from 'react';
import {useState} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { ActivityIndicator, Alert } from 'react-native';
 import DateTimePicker from '@react-native-community/datetimepicker'; 
 import {CheckBox,ListItem,Body} from 'native-base';
 import {prepaid} from "../../helper";
 import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    Linking,
    useColorScheme,
    TouchableOpacity,
    TextInput,
    Image,
    Icon,
    Button,
    View,
  } from 'react-native';
import {Picker} from '@react-native-community/picker'

import { color } from 'react-native-reanimated';
import axios from 'axios';
import { Authheader } from '../../keys';
import AppContext from '../../AppContext';

const PinEditpending=({route,navigation})=>{
    const [date, setDate] = useState(new Date(Date.now()));

    const { customer_mobi,customer_nam,order_id,initial,type,oid,date2,pin_code} = route.params;

  const [mode, setMode] = useState('date');
  const [selectedValue, setSelectedValue] = useState("Cash");
  const [phone,setphone] = useState("")
  const [amount,setamount] = useState("")
  const [show, setShow] = useState(false);
  const [showbutton,set_showbutton]=useState(false);
  const myContext=React.useContext(AppContext)
  const [ccheckbox,set_checkbox]=useState(false)

   
    
      
    
      
     const sendpayment = () =>{
      

       const payload = {
        _id:order_id,
        invoice:amount
       }
       const payload2 = {
        phone: parseInt(customer_mobi),
        amount:parseInt(initial),
        mode_of_payment:type,
        date_of_payment: date2.toString(),
        community_id: myContext.community,
      }
       if(type==prepaid){
         axios.post("http://35.154.70.53:5000/payment/post",payload2,Authheader)
         .then(res =>{
          console.log("sent")
        }).then(()=>{
          axios.put("http://35.154.70.53:5000/order/update",payload,Authheader)
        .then(res => {
          Linking.openURL("https://wa.me/91"+customer_mobi+"?text=delivery%0a%0aHello "+customer_nam+" Ji. Your order "+oid+ " with final amount "+amount+" is delivered.%0a "+"Thank you.%0a From Chotu")
            navigation.navigate('pinpending',{pin:pin_code})
        })
        .catch(err=>{
          console.log("error")
        })
        })
        .catch(err=>{
          console.log(err)
        })
       }
       else{
        axios.put("http://35.154.70.53:5000/order/update",payload,Authheader)
        .then(res => {
          Linking.openURL("https://wa.me/91"+customer_mobi+"?text=delivery%0a%0aHello "+customer_nam+" Ji. Your order "+oid+ " with final amount "+amount+" is delivered.%0a "+"Thank you.%0a From Chotu")
          navigation.navigate('pinpending',{pin:pin_code})
        })
        .catch(err=>{
          console.log("error")
   })
       }
       
       

     }


     const changecheck=(e)=>{
       set_showbutton(true)
       if(ccheckbox==true)
        set_checkbox(false);
       else{
         set_checkbox(true);
       }
      if(ccheckbox==true){
        
        setamount("0")
      }
      else{
        
        setamount(initial.toString())
      }
    }
    
    
    
    if(showbutton){
      return (
       
          <ScrollView  >
        <View style={{backgroundColor:"#F2F8FF",height:hp("100%")}}>
       
            <View style={{backgroundColor:"#0E4C75",flexDirection:'row',height:hp("42%"),width:wp("100%"),justifyContent:"center",textAlign:'center'}}>
                <View style={{justifyContent:"center",textAlign:'center',bottom:hp("1.5%"),top:("2%")}}>
                      <Image style={{width:wp("38%"),height:hp("18%"),justifyContent:"center",bottom:hp("1%"), resizeMode: 'contain',}} source={require('../assets/images/feed_img.png')}/>
                <View style={{bottom:hp("5.5%"),}}>
                <Text style ={{fontSize:wp("5%"),fontWeight:'bold',color:'#FFFFFF',textAlign:'center',}}>{customer_nam}</Text>
                <Text style ={{fontSize:wp("3%"),color:'#E5E5E5',textAlign:'center',}}>{customer_mobi}</Text>
                <Text style ={{fontSize:wp("3%"),color:'#FFFFFF',textAlign:'center',}}>{type}</Text>
                <Text style ={{fontSize:wp("6%"),color:'#FFFFFF',fontWeight:'bold',textAlign:'center',}}>₹200</Text>
                <TouchableOpacity style={{width: wp("30%"),height:hp("4.8%"),marginLeft:wp("4%") ,backgroundColor:'#FFFFFF', borderRadius:8,}}>
                   <Text style={{textAlign:'center', color:'#000000',top:hp("1.3%"),fontWeight:'bold'}}>Order datails</Text>
               </TouchableOpacity>
                </View>
               
                
            </View>
          </View>




          {/* <View style={{marginLeft:30,marginTop:30}}>
          <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,fontSize:15}}>Name </Text>
              <Text style={{flex:5,fontSize:15}}>{customer_nam}</Text>
          </View>
  
          <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,fontSize:15}}>Phone No </Text>
              <Text style={{flex:4,fontSize:15}}>{customer_mobi}</Text>
          </View>
  
          <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,fontSize:15}}>Order Id </Text>
              <Text style={{flex:4,fontSize:15}}>{oid}</Text>
          </View>
  
          <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,fontSize:15}}>Payment mode </Text>
              <Text style={{color:"red",flex:4,fontSize:15}}>{type}</Text>
          </View>
          </View> */}
  
  
          <View style={{ marginLeft:wp("6%"),marginRight:wp("6%"),bottom:hp("2%")}}>
              {/* <Text style={{textAlign:"center",fontSize:30,marginTop:20,marginBottom:10,fontWeight:"bold",color:'#0f4c75'}}>Accept pending order</Text> */}
             <View style={{top:hp("5%"),left:wp("1%")}}>
           <Text style={{textAlign:"left",fontSize:wp("4.5%"),color:'#0f4c75',fontWeight:"bold" }}>Invoice Amount</Text>
           </View>
           
           
          
                
            <View style={{flexDirection:"row",marginTop: hp("5.5%"),}}>
           
             <TextInput
             keyboardType="numeric"
             style={styles2s.input}
               color="black"/>

            </View>
  
  
              {/* <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,color:"#003366",height:55,marginTop:22,fontSize:16,marginLeft:6,fontWeight:"bold"}}>Ordered Amount:</Text>
              
               <Text style={styles2.input2}>{initial}</Text>
               
              </View> */}
  
              {/* <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,color:"#003366",height:55,marginTop:10,fontSize:16,marginLeft:6,fontWeight:"bold"}}>Invoice Amount:</Text>
               <TextInput
               keyboardType="numeric"
          style={styles2.input}
          onChangeText = {e => {set_showbutton(true);setamount(e)}}
          value={amount}
  />
              </View> */}
             
  
  
              
              
               
              <ListItem>
              <CheckBox style={{borderColor:'#000000', borderRadius:4}} checked={ccheckbox} onPress={e=>changecheck(e)}/>
              <Body>
                <Text style={{textAlign:"center",fontSize:wp("3.5%"),color:'#0f4c75',fontWeight:"bold" }}> Order and Invoice amount are same</Text>
              </Body>
            </ListItem>
              
  {/* <View style={{marginTop:40,width:150,alignContent:"center",marginLeft:100}}> */}
                 <TouchableOpacity 
            // onPress={() => { navigation.navigate('Refundform', { order_id:a._id }) }}
            style={{top:hp("3%"),backgroundColor:"#0f4c75",padding:15,borderRadius:12}}>
                <Text style={{fontWeight:"bold",color:"#FFFFFF",textAlign:"center",fontSize:15}}>Delivery</Text>
            </TouchableOpacity>

      {/* </View>         */}
 
              
  
              {/* <View style={{marginTop:40,width:150,alignContent:"center",marginLeft:100}}>
              
              <Button color='#0f4c75' title="submit" disabled={true} onPress ={()=>
               Alert.alert(
                "Order Delivery",
                "Are you sure want to sumbit",
                [
                  
                  { text: "OK", onPress: () => sendpayment() },
                  {text: "cancel",onPress:()=>console.log("delivery not made")}
                ]
              )
            } ></Button>
              
              </View> */}
              
          </View>
           
          </View>
            </ScrollView>
     
      
  //       <View style={{backgroundColor:"#F2F8FF",height:'100%'}}>

          




  //         <View style={{marginLeft:30,marginTop:30}}>
  //         <View style={{flexDirection:"row"}}>
  //             <Text style={{flex:1,fontSize:15}}>Name</Text>
  //             <Text style={{flex:5,fontSize:15}}>{customer_nam}</Text>
  //         </View>
  
  //         <View style={{flexDirection:"row"}}>
  //             <Text style={{flex:1,fontSize:15}}>Phone No </Text>
  //             <Text style={{flex:4,fontSize:15}}>{customer_mobi}</Text>
  //         </View>
  
  //         <View style={{flexDirection:"row"}}>
  //             <Text style={{flex:1,fontSize:15}}>Order Id </Text>
  //             <Text style={{flex:4,fontSize:15}}>{oid}</Text>
  //         </View>
  
  //         <View style={{flexDirection:"row"}}>
  //             <Text style={{flex:1,fontSize:15}}>Payment mode </Text>
  //             <Text style={{color:"red",flex:4,fontSize:15}}>{type}</Text>
  //         </View>
  //         </View>
  
  
  //         <View style={{ marginLeft:24,marginRight:24,marginTop:40,marginBottom:40}}>
  //             <Text style={{textAlign:"center",fontSize:30,marginTop:20,marginBottom:10,fontWeight:"bold",color:'#0f4c75'}}>Accept pending order</Text>
             
  
  
  //             <View style={{flexDirection:"row"}}>
  //             <Text style={{flex:1,color:"#003366",height:55,marginTop:22,fontSize:18,marginLeft:6,fontWeight:"bold"}}>Ordered Amount:</Text>
              
  //              <Text style={styles2.input2}>{initial}</Text>
               
  //             </View>
  
  //             <View style={{flexDirection:"row"}}>
  //             <Text style={{flex:1,color:"#003366",height:55,marginTop:10,fontSize:18,marginLeft:6,fontWeight:"bold"}}>Invoice Amount:</Text>
  //              <TextInput
  //              keyboardType="numeric"
  //         style={styles2.input}
  //         onChangeText = {e => setamount(e)}
  //         value={amount}
  // />
  //             </View>
             
  
  
              
              
               
  //             <ListItem>
  //             <CheckBox checked={ccheckbox} onPress={e=>changecheck(e)}/>
  //             <Body>
  //               <Text>Ordered Amount = Invoice Amount</Text>
  //             </Body>
  //           </ListItem>
              
  
  
              
  
              
  
  //             <View style={{marginTop:40,width:150,alignContent:"center",marginLeft:100}}>
              
  //             <Button color='#0f4c75' title="submit" disabled={false} onPress ={()=>
  //              Alert.alert(
  //               "Order Delivery",
  //               "Are you sure want to submit",
  //               [
                  
  //                 { text: "OK", onPress: () => sendpayment() },
  //                 {text: "cancel",onPress:()=>console.log("delivery not made")}
  //               ]
  //             )
  //           } ></Button>
              
  //             </View>
              
  //         </View>
  //         </View>
  
  
      )
    }
    else{
      return (
        
      <ScrollView >
        <View  style={{backgroundColor:"#F2F8FF",height:hp("100%")}}>
            <View style={{backgroundColor:"#0E4C75",flexDirection:'row',height:hp("42%"),width:wp("100%"),justifyContent:"center",textAlign:'center'}}>
                <View style={{justifyContent:"center",textAlign:'center',bottom:hp("1.5%"),top:("2%")}}>
                      <Image style={{width:wp("38%"),height:hp("18%"),justifyContent:"center",bottom:hp("1%"), resizeMode: 'contain',}} source={require('../assets/images/feed_img.png')}/>
                <View style={{bottom:hp("5.5%"),}}>
                <Text style ={{fontSize:wp("5%"),fontWeight:'bold',color:'#FFFFFF',textAlign:'center',}}>{customer_nam}</Text>
                <Text style ={{fontSize:wp("3%"),color:'#E5E5E5',textAlign:'center',}}>{customer_mobi}</Text>
                <Text style ={{fontSize:wp("3%"),color:'#FFFFFF',textAlign:'center',}}>{type}</Text>
                <Text style ={{fontSize:wp("6%"),color:'#FFFFFF',fontWeight:'bold',textAlign:'center',}}>₹200</Text>
                <TouchableOpacity style={{width: wp("30%"),height:hp("4.8%"),marginLeft:wp("4%") ,backgroundColor:'#FFFFFF', borderRadius:8,}}>
                   <Text style={{textAlign:'center', color:'#000000',top:hp("1.3%"),fontWeight:'bold'}}>Order datails</Text>
               </TouchableOpacity>
                </View>
               
                
            </View>
          </View>




          {/* <View style={{marginLeft:30,marginTop:30}}>
          <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,fontSize:15}}>Name </Text>
              <Text style={{flex:5,fontSize:15}}>{customer_nam}</Text>
          </View>
  
          <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,fontSize:15}}>Phone No </Text>
              <Text style={{flex:4,fontSize:15}}>{customer_mobi}</Text>
          </View>
  
          <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,fontSize:15}}>Order Id </Text>
              <Text style={{flex:4,fontSize:15}}>{oid}</Text>
          </View>
  
          <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,fontSize:15}}>Payment mode </Text>
              <Text style={{color:"red",flex:4,fontSize:15}}>{type}</Text>
          </View>
          </View> */}
  
  
          <View style={{ marginLeft:wp("6%"),marginRight:wp("6%"),bottom:hp("2%")}}>
              {/* <Text style={{textAlign:"center",fontSize:30,marginTop:20,marginBottom:10,fontWeight:"bold",color:'#0f4c75'}}>Accept pending order</Text> */}
             <View style={{top:hp("5%"),left:wp("1%")}}>
           <Text style={{textAlign:"left",fontSize:wp("4.5%"),color:'#0f4c75',fontWeight:"bold" }}>Invoice Amount</Text>
           </View>
           
           
          
                
            <View style={{flexDirection:"row",marginTop: hp("5.5%"),}}>
           
             <TextInput
             keyboardType="numeric"
             style={styles2s.input}
               color="black"/>

            </View>
  
  
              {/* <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,color:"#003366",height:55,marginTop:22,fontSize:16,marginLeft:6,fontWeight:"bold"}}>Ordered Amount:</Text>
              
               <Text style={styles2.input2}>{initial}</Text>
               
              </View> */}
  
              {/* <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,color:"#003366",height:55,marginTop:10,fontSize:16,marginLeft:6,fontWeight:"bold"}}>Invoice Amount:</Text>
               <TextInput
               keyboardType="numeric"
          style={styles2.input}
          onChangeText = {e => {set_showbutton(true);setamount(e)}}
          value={amount}
  />
              </View> */}
             
  
  
              
              
               
              <ListItem>
              <CheckBox style={{borderColor:'#000000', borderRadius:4}} checked={ccheckbox} onPress={e=>changecheck(e)}/>
              <Body>
                <Text style={{textAlign:"center",fontSize:wp("3.5%"),color:'#0f4c75',fontWeight:"bold" }}> Order and Invoice amount are same</Text>
              </Body>
            </ListItem>
              
  {/* <View style={{marginTop:40,width:150,alignContent:"center",marginLeft:100}}> */}
                 <TouchableOpacity 
            // onPress={() => { navigation.navigate('Refundform', { order_id:a._id }) }}
            style={{top:hp("3%"),backgroundColor:"#0f4c75",padding:15,borderRadius:12}}>
                <Text style={{fontWeight:"bold",color:"#FFFFFF",textAlign:"center",fontSize:15}}>Delivery</Text>
            </TouchableOpacity>

      {/* </View>         */}
 
              
  
              {/* <View style={{marginTop:40,width:150,alignContent:"center",marginLeft:100}}>
              
              <Button color='#0f4c75' title="submit" disabled={true} onPress ={()=>
               Alert.alert(
                "Order Delivery",
                "Are you sure want to sumbit",
                [
                  
                  { text: "OK", onPress: () => sendpayment() },
                  {text: "cancel",onPress:()=>console.log("delivery not made")}
                ]
              )
            } ></Button>
              
              </View> */}
              
          </View>
          </View>
        </ScrollView>
  
      )
    }
    
}

const styles2s = StyleSheet.create({
    input: {
      // height: hp("3.5%"),
      marginBottom:hp("1.5%"),
      borderWidth: 1,
      borderRadius:8,
      borderColor:"#0f4c75",
      borderBottomWidth: 1,
      flex:1,
      backgroundColor: '#FFFFFF',
     
    },
  });
const styles2 = StyleSheet.create({
    input: {
      height: 40,
      marginTop: 12,
      marginBottom:12,
      marginLeft:6,
      marginRight:6,
      borderWidth: 1,
      borderRadius:10,
      borderColor:'#0f4c75',
      flex:2.8
    },
    input2: {
        height: 40,
        paddingTop:8,
        paddingLeft:8,
        marginTop: 28,
        marginBottom:12,
        marginLeft:20,
        marginRight:6,
        borderWidth: 1,
        borderRadius:10,
        borderColor:'#0f4c75',
        flex:2.8
      },
  });


  const styles = StyleSheet.create({
    checkboxContainer: {
      flexDirection: "row",
      marginBottom: 20,
    },
    checkbox: {
      alignSelf: "center",
    },
    label: {
      margin: 8,
    },
    container: {
      flex: 1,
      margin: 10,
      marginTop: 30,
      padding: 30,
      
    },
    
    buttonFacebookStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      flex:1,
      borderWidth: 0.5,
      borderColor: '#fff',
      height: 40,
      width:35,
      borderRadius: 5,
      margin: 5,
    },
    buttonImageIconStyle: {
      padding: 10,
      margin: 5,
      height: 25,
      width: 25,
      resizeMode: 'stretch',
    },
    buttonTextStyle: {
      color: '#fff',
      marginBottom: 4,
      marginLeft: 10,
    },
    buttonIconSeparatorStyle: {
      backgroundColor: '#fff',
      width: 1,
      height: 40,
    },
  });

  const styles3 = StyleSheet.create({
    container: {
        flex:4,
      paddingTop: 8,
      alignItems: "center"
    }
  });

  const styles4=StyleSheet.create({
    container: {
      
      
      paddingHorizontal: 24,
      paddingBottom:  18,
      
    },
    logoApp: {
      marginTop: 50,
      alignSelf: "center",
      alignItems: "center",
    },
    logo: {
      height:100,
      width:100,
      marginBottom: 12,
      
    },
    inputLogin:{
      marginTop:50,
    }

  })
  


export default PinEditpending;
