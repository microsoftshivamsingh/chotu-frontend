import * as React from 'react';
import {useState} from 'react';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { NavigationContainer } from '@react-navigation/native';
import { ActivityIndicator, Alert } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker'; 
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    TouchableOpacity,
    Linking,
    CheckBox,
    TextInput,
    Image,
    Icon,
    Button,
    View,
  } from 'react-native';
import {Picker} from '@react-native-community/picker'
import { color } from 'react-native-reanimated';
import axios from 'axios';
import { Authheader } from '../../keys';
import AppContext from '../../AppContext';

const Scre=({route,navigation})=>{
    const [date, setDate] = useState(new Date(Date.now()));
    const { customer_mobi,customer_nam} = route.params;
    
  const [mode, setMode] = useState('date');
  const [selectedValue, setSelectedValue] = useState("cash");
  const [phone,setphone] = useState("")
  const [amount,setamount] = useState("")
  const [show, setShow] = useState(false);

  const [paykey,set_paykey]=useState(0);
  const myContext=React.useContext(AppContext)
  var radio_props = [
    {label: 'cash', value: 0 },
    {label: 'Gpay', value: 1 },
    {label: 'Phonepe', value: 2 },
    {label: 'paytm', value: 3 },
    {label:'Netbanking',value:4},
    {label:'other',value:5}
  ];
  

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
      };
    
      const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
      };
    
      const showDatepicker = () => {
          
        showMode('date');
      };
     const sendpayment = () =>{
       const payload = {
         phone: parseInt(customer_mobi),
         amount:parseInt(amount),
         mode_of_payment:selectedValue,
         date_of_payment: date.toString(),
         community_id: myContext.community
       }
       axios.post("http://35.154.70.53:5000/payment/post",payload,Authheader)
       .then(res => {
        Linking.openURL("https://wa.me/91"+customer_mobi+"?text=payment%0a%0aHello "+customer_nam+" Ji. Your payment of "+amount+ " from "+selectedValue+" is recieved.%0a"+"Thank you.%0aFrom Chotu")
        navigation.navigate('Ledger',{customer_mob:customer_mobi,customer_name:customer_nam})
       })
       .catch(err=>{
         console.log("error")
       })

     }

     const pay=(e)=>{
        set_paykey(e);
        let temp="";
        radio_props.forEach(a=>{
          if(a.value==e){
           
            temp=a.label
          }
        })
       
        setSelectedValue(temp)
     }
     
    return (
      // <ScrollView>
      <View style={{backgroundColor:"#F2F8FF",height:'100%'}}>
      
        <View style={{backgroundColor:"#0E4C75",flexDirection:'row',height:hp("15%"),width:wp("100%"),}}>
        <View style={{width:wp("100%"),top:hp("8%"),marginHorizontal:wp("5%")}}>
          <Text style={{fontSize:wp("7%"),fontWeight:'bold',color:'#FFFFFF'}}>{customer_nam}</Text>
          <Text style={{fontSize:wp("3%"),color:'#F2F8FF'}}> {customer_mobi}</Text>
        </View>
          

        </View>

         {/* <View style={styles.amount}>
                
                <Image
                    source={require("../assets/images/rupee.png")}
                    
                   />
                </View> */}
        <View style={{ marginLeft:wp("6%"),marginRight:wp("6%"),}}>
           <View style={{top:hp("5%"),left:wp("1%")}}><Text style={{textAlign:"left",fontSize:wp("4.5%"),color:'#0f4c75', fontWeight:'bold'}}>Enter Amount</Text>
           </View>
           
           
          
                
            <View style={{flexDirection:"row"}}>
           
             <TextInput
             keyboardType="numeric"
        style={styles2.input}
        
        onChangeText = {e => setamount(e)}
        value={amount}
/>
            </View>

           
            

             
            <View style={{flexDirection:'row',marginRight:wp("5.5%"),}}>

                <TouchableOpacity
                style={styles.buttonFacebookStyle}
                onPress={showDatepicker}>
                
                <Image
                    source={require("../assets/images/calender.png")}
                    style={styles.buttonImageIconStyle}
                   
                />
                </TouchableOpacity>
            {/* <View style={styles.buttonIconSeparatorStyle} /> */}
            {show && (
                <DateTimePicker
                value={date}
                color='#FFFFFF'
                display='default'
                mode={mode}
                onChange={onChange}
                />
            )}
            
            <Text style={styles.date} >{date.toString().substr(4,12)}</Text>
            </View>

            

      <View style={styles3.grid}>
            <View style={styles3.box}>
             <TouchableOpacity>
                <Image source={require("../assets/images/GPay.png")}/>
             </TouchableOpacity>
           </View>

           <View style={styles3.box}>
             <TouchableOpacity>
                <Image source={require("../assets/images/PhonePe.png")}/>
             </TouchableOpacity>
          </View>
          <View style={styles3.box}>
             <TouchableOpacity>
                <Image source={require("../assets/images/Paytm.png")}/>
             </TouchableOpacity>
           </View>

           <View style={styles3.box}>
             <TouchableOpacity>
                <Image source={require("../assets/images/Cash.png")}/>
             </TouchableOpacity>
          </View>
          <View style={styles3.box}>
             <TouchableOpacity>
                <Image source={require("../assets/images/Net.png")}/>
             </TouchableOpacity>
           </View>

           <View style={styles3.box}>
             <TouchableOpacity>
                <Image source={require("../assets/images/Other.png")}/>
             </TouchableOpacity>
          </View>
      </View>
            

{/* 
            <View style={{fontSize:10,marginLeft:20,top:40}}>
            <RadioForm
              radio_props={radio_props}
              value={paykey}
              buttonSize={10}
              buttonOuterSize={0}
              onPress={(value) => {pay(value)}}
            />
          </View> */}

             {/* <View style={{top:hp("5%"),left:wp("1%")}}><Text style={{textAlign:"left",fontSize:wp("4.5%"),color:'#0f4c75', fontWeight:'bold'}}>Enter method</Text>
           </View>
           
           
          
                
            <View style={{flexDirection:"row"}}>
           
             <TextInput
             keyboardType="numeric"
        style={styles2.input}
        
        onChangeText = {e => setamount(e)}
        value={amount}
/>
            </View> */}

            <View style={{top:hp("5%"),}}>
            <Button color='#0f4c75' title="Submit Payment"
            style={{marginRight:10,marginLeft:10}}
             onPress ={()=>
               Alert.alert(
                "New Payment",
                "Are you sure want to add the amount",
                [
                  
                  { text: "OK", onPress: () => sendpayment() },
                  {text: "cancel",onPress:()=>console.log("payment not made")}
                ]
              )
            } >

            </Button>
            </View>
            
        </View>
        </View>

        // </ScrollView>
    )
}
const styles2 = StyleSheet.create({
    input: {
      height: hp("5.5%"),
      marginTop: hp("5.5%"),
      marginBottom:hp("1.5%"),
      // borderLeftWidth:0,
      // marginLeft:wp("0.5%"),
      // marginRight:wp("0.5%"),
      borderWidth: 1,
      borderRadius:5,
      borderColor:"#0f4c75",
      flex:1,
    },
  });


  const styles = StyleSheet.create({
    
    container: {
      flex: 1,
      margin: 10,
      marginTop: 30,
      padding: 30,
      
    },
   

    date:{
        // textAlignVertical:"center",
        borderBottomWidth:1,
        borderTopWidth:1,
        borderRightWidth:1,
        borderBottomRightRadius:8,
        borderTopRightRadius:8,
        // borderColor:"#0f4c75",
        textAlign:'center',
        paddingTop:wp("3.4%"),
        height: hp("5.5%"),
        // right:wp("10%"),
        // height:39,
        // padding: 10,
  
        // justifyContent:'center',
        // position:"relative",
        marginLeft:wp("-5%"),
        left:wp("5%"),
        paddingRight:wp("10%"),
        flex:1
    },
     amount:{
        backgroundColor:"#0E4C75",
        borderTopWidth:2,
        //  flexDirection: 'row',
        // flex:0.2,
        // borderBottomWidth:5,
        borderBottomLeftRadius:8,
        borderTopLeftRadius:8,
        borderLeftWidth:2,
      
        top:52.6,
        paddingBottom:29,
        marginTop:1,
        // marginLeft:10,
        paddingTop:10,
        paddingLeft:23,
        width:60,
        height: 40,
        left:2,
        flexDirection: 'row',
        borderColor:"#0f4c75",
        // margin: 1,
    },
    buttonFacebookStyle: {
      backgroundColor:"#0E4C75",
      borderBottomWidth:2,
      borderTopWidth:2,
      flexDirection: 'row',
      // alignItems: 'center',
      flex:0.3,
      marginTop:hp("-0.1%"),
      borderLeftWidth:2,
      borderColor:"#0f4c75",
      height: hp("5.6%"),
     borderBottomLeftRadius:8,
     borderTopLeftRadius:8,
      margin: wp("-0.1%"),
      // marginRight:10
      // width:1000,
    },
    buttonImageIconStyle: {
      // padding: 8,
      marginLeft:wp("6%"),
      marginTop:hp("1.4%"),
      height: hp("2.3%"),
      width: wp("5%"),
      resizeMode: 'stretch',
    },
    buttonTextStyle: {
      color: '#fff',
      marginBottom: 4,
      marginLeft: 10,
    },
    buttonIconSeparatorStyle: {
      backgroundColor: '#000000',
      width: 1,
      height: 40,
    },
  });

  const styles3 = StyleSheet.create({
    container: {
        flex:1,
      paddingTop: 100,
      alignItems: "center"
    },
     grid: {
     
    // left: wp("5%"),
    right: wp("3%"),
    flexDirection: "row",
    // alignContent: "center",
    justifyContent: "space-evenly",
    flexWrap:"wrap"
       
  },
    box:{
   

     width: wp("25%"),
     height: hp("11%"),
     borderRadius:24,
     marginTop:hp("2.5%"),

   
    }
  }
);
  


export default Scre;



{/* <View style={{flexDirection:'column',alignItems:'center'}}>
        
        <View style={{margin:20, borderColor:'#0f4c75',borderWidth:2,borderRadius:5,}}>
          <View style={{marginLeft:30,marginTop:10}}>
          <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,fontSize:15}}>Name </Text>
              <Text style={{flex:5,fontSize:15}}>{customer_nam}</Text>
          </View>
          <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,fontSize:15}}>PhoneNo </Text>
              <Text style={{flex:4,fontSize:15}}>{customer_mobi}</Text>
          </View>
  
          </View>
          <View style={{ marginLeft:24,marginRight:24,marginTop:20,marginBottom:40}}>
              <Text style={{textAlign:"center",fontSize:30,marginTop:10,marginBottom:10,fontWeight:"bold",color:'#0f4c75'}}>Add New Payment</Text>
              
              
  
              <View style={{flexDirection:"row"}}>
              <Text style={{flex:1,color:"#003366",height:40,marginTop:18,fontSize:18,marginLeft:6,fontWeight:"bold"}}>Amount:</Text>
               <TextInput
               keyboardType="numeric"
          style={styles2.input}
          
          onChangeText = {e => setamount(e)}
          value={amount}
  />
              </View>
             
              
  
               
              <View style={{flexDirection:'row'}}>
  
              <TouchableOpacity
                  style={styles.buttonFacebookStyle}
                  
                  onPress={showDatepicker}
                  >
                  
                  <Image
                      source={{
                      uri:
                          'https://cdn3.iconfinder.com/data/icons/computers-programming/512/datepicker-512.png',
                      }}
                      style={styles.buttonImageIconStyle}
                     
                  />
                   
                  <View style={styles.buttonIconSeparatorStyle} />
                  
              </TouchableOpacity>
              {show && (
                  <DateTimePicker
                  value={date}
                  
                  display='default'
                  mode={mode}
                  onChange={onChange}
                  />
              )}
              
              <Text style={{flex:8,textAlignVertical:"center",borderWidth:1,borderRadius:10,borderColor:"#0f4c75",marginLeft:6,marginRight:6,paddingLeft:10}} >{date.toString().substr(4,12)}</Text>
              </View>
  
              
  
  
              
  
              
  
  
              <View style={{fontSize:10,marginLeft:20}}>
              <RadioForm
                radio_props={radio_props}
                value={paykey}
                buttonSize={10}
                buttonOuterSize={0}
                onPress={(value) => {pay(value)}}
              />
            </View>
  
            
  
  
  
              <View style={{marginTop:10,marginBottom:20,width:150,alignContent:"center",marginLeft:100}}>
              <Button color='#0f4c75' title="submit" onPress ={()=>
                 Alert.alert(
                  "New Payment",
                  "Are you sure want to add the amount",
                  [
                    
                    { text: "OK", onPress: () => sendpayment() },
                    {text: "cancel",onPress:()=>console.log("payment not made")}
                  ]
                )
              } ></Button>
              </View>
              
          </View>
          </View>
  
          </View> */}
