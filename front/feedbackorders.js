import axios from 'axios';
import React,{useState,useEffect, useContext} from 'react';
import PTRView from 'react-native-pull-to-refresh';
import AppContext from '../../AppContext';
//import { SafeAreaView, StyleSheet, TextInput } from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { Authheader } from '../../keys';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Button,
    Text,
    TextInput,
    View,
    Linking,
    TouchableOpacity,
    Image
  } from 'react-native'; 
import { exp } from 'react-native-reanimated';

const Feedbackorders=({route,navigation})=>{
    const [orders,set_orders]=useState([]);
    const { customer_mob,customer_name} = route.params;
  const myContext=React.useContext(AppContext);

  const fetch_order_details = async () =>{

    
    
    
    
    axios.get(`http://35.154.70.53:5000/order/customer_fetch?mobile=${customer_mob}&community_id=${myContext.community}`,Authheader)
    .then(res => {
      
      
      console.log(orders.length)
      let temp = []
      res.data.forEach(a=>{
        //console.log(a)
        if(a.invoice!=null){
          temp.push(a)
        }
      })
      
      set_orders([...temp])
     // console.log("from order",orders.length)
    
    })
    .catch(err=>{
        console.log(err)
    })
  
  
  }

  useEffect( () => {
    const unsubscribe = navigation.addListener('focus', () => {
         fetch_order_details();
      });
      return 
    }, [navigation]);
  
  
  
  const _refresh =  ()=> {
    return new Promise((resolve) => {
       fetch_order_details()
       .then(r => resolve())
      
    });
  }

  const compare = (a, b) => {
    const ad = new Date(a.date_of_order)
    const bd = new Date(b.date_of_order)
    if (ad < bd)
      return 1;
    else
      return -1;

  }
  var weekday = new Array(7);
weekday[0] = "Sun";
weekday[1] = "Mon";
weekday[2] = "Tue";
weekday[3] = "Wed";
weekday[4] = "Thu";
weekday[5] = "Fri";
weekday[6] = "Sat";

return(
 
    <PTRView onRefresh={_refresh} >
 
          <View style={{backgroundColor:"#0E4C75",flexDirection:'row',height:hp("25%"),width:wp("100%"),justifyContent:"center",textAlign:'center'}}>
            <View style={{justifyContent:"center",textAlign:'center'}}>
                <Image style={{width:wp("44%"),height:hp("18%"),justifyContent:"center",}} source={require('../assets/images/feed_img.png')}/>
                <View style={{bottom:hp("4.5%")}}>
                <Text style ={{fontSize:wp("5%"),fontWeight:'bold',color:'#FFFFFF',textAlign:'center',}}>{customer_name}</Text>
                <Text style ={{fontSize:wp("3%"),color:'#FFFFFF',textAlign:'center',}}>{customer_mob}</Text>
                </View>
            </View>
        </View>
        
        <ScrollView  style={{backgroundColor:"#F2F8FF",height:hp('100%')}}>
   { orders.sort(compare).map((a, indx) => {
        let tempdate=new Date(a.date_of_order).getDay();
        let day=weekday[tempdate];
        let finaldate=a.date_of_order.substr(0, 10)+" ("+day+")";
        return (
            <View >

       
          <View>
               
            <TouchableOpacity style = {styles5.container} onPress={() => { navigation.navigate('Refundform', { order_id:a._id }) }}>

                <Image source={require('../assets/images/feed_img.png')}/>
                <View style={{bottom:hp("0.4%")}}>
              
                  <Text style ={{fontSize:24,fontWeight:'bold', color:"#0E4C75"}}>ID:{a.id}</Text>
                
                  <Text style={{fontSize:11,color:"#0E4C75"}}>{a.date_of_order.substr(0, 10)}{" | ₹"}{a.total}</Text>
                </View>
    
               <Image style={{width:wp("5%"),height:hp("1.7%"),position:'absolute',right:wp("6%")}} source={require('../assets/images/singlesmall.png')}/>

            </TouchableOpacity>
            
            </View>
                </View>
        
    )
    
    
}
    )}
    </ScrollView>


  

    </PTRView>
       
)
}

export default Feedbackorders;
const styles5=StyleSheet.create({
    container:{
      padding:20,
    backgroundColor:'#FFFFFF',
    borderRadius:10,
    marginTop:10,
    marginLeft:15,
    marginRight:15,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "blue",
    shadowRadius: 4,
    elevation: 6,
    marginBottom:20,
    }
  })
