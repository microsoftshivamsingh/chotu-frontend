import axios from 'axios';
import React,{useState,useEffect, useContext} from 'react';
import PTRView from 'react-native-pull-to-refresh';
import AppContext from '../../AppContext';
//import { SafeAreaView, StyleSheet, TextInput } from "react-native";
import { Card, Left,CardItem,Icon,Right, Body } from 'native-base';
import { Authheader } from '../../keys';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Button,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    Image
  } from 'react-native'; 

const Allpins=({navigation})=>{
    const myContext=useContext(AppContext);
    const [pin_list,set_pin_list]=useState([]);
     const [search_phrase,set_search_phrase] = useState("");
    const fetch_pin_details = async () => {
       
        axios.get(`http://35.154.70.53:5000/pin/fetch?community_id=${myContext.community}`,Authheader)
        .then(res =>{
            
            //console.log("Axios response received data: ",res.data);
            set_pin_list([...res.data]);
            console.log("backend req completed from pin");

        }).catch(err => {
            if(err.response)
            {   if (err.response.data) {
                console.log(err.response.data.errors);
                err.response.data.errors.map((e) => {
                   
                   console.log("debug e is ", e);
                   return e;
                }); }
                else{
                    console.log("Network error");
                }
            }
            else if (err.request){
                console.log("2 error->", err.message);
            }
            else {
                // Something happened in setting up the request and triggered an Error
                console.log('e Error is ', err.message);
              }
            
        })
    }

    useEffect( () => {
        const unsubscribe = navigation.addListener('focus', () => {
             fetch_pin_details();
          });
          return unsubscribe;
        }, [navigation]);
    

    const _refresh =  ()=> {
        return new Promise((resolve) => {
           fetch_pin_details()
           .then(r => resolve())
          
        });
      }


    return(
        <PTRView onRefresh={_refresh} style={{backgroundColor:"#F2F8FF",height:'100%'}}>
        {/* style={{marginLeft:12,marginTop:20,marginRight:12}} */}
            <ScrollView style={{marginLeft:wp("4%"),marginRight:wp("3%")}}>
            {/* <View style={{flexDirection:"row",justifyContent:"center"}}>
            <View style={{ width:130}}>
            <Button color='#0f4c75' title="refresh" onPress={()=>_refresh()}></Button>
            </View>
            </View> */} 
            <View>
            <View style={{marginTop:5,marginBottom:30, marginTop:50,}}>
                <View
                style={{borderColor:"#F0F0F0"}}
                activeOpacity={0.7}
                >
                  
                  <View style={{flexDirection:"row"}}>

                  <TextInput
                   placeholder="Search Contact"

                   placeholderTextColor='#000000'
                   value={search_phrase}
                   onChangeText={e=>set_search_phrase(e)}
                  
                style={{flex:1,backgroundColor:'#FFFFFF',borderRadius:8,borderWidth:1,fontFamily:"Muli_600SemiBold",fontSize:15,borderColor:"#F0F0F0",color:"#000000",paddingLeft:20}}
                //   borderColor="#F0F0F0"
                  editable={true}>

                  </TextInput >
                  <TouchableOpacity  style={{marginTop:12,right:20,position:'absolute'}}>
                  <Image  source={require('../assets/images/ic_search_normal.png')}/>
                  </TouchableOpacity>
                  </View>
                </View>
            </View>

            
                {
                    pin_list.map((pin,indx)=>{
                        if(pin.code.toLowerCase().includes(search_phrase.toLowerCase()) || pin.code.includes(search_phrase))
                        {  
                            if(true){

                                    return(
                            /* <Card bordered key={indx}  >
                        <CardItem button onPress={() => {navigation.navigate('pinpending',{pin:pin.code})}}  >
                            <Body>
                            <Left>
                            <Text style={{color:"#0f4c75",fontWeight:"bold"}}>Vendor name: {pin.code}</Text>
                            <Text>Delivery Date:</Text>
                            <Text >Pending:{pin.isPending}</Text>
                            
                            </Left>
                            </Body>
                            <Right>
                            <Icon name="arrow-forward" />
                            
                            
                            </Right>
                            
                            </CardItem>
                            </Card> */
                            <TouchableOpacity key={indx} style={styles5.container} onPress={() => {navigation.navigate('pinpending',{pin:pin.code})}}>
                            <Image  source={require('../assets/images/feed_img.png')}/>
                            {/*  style={{marginLeft:20,width:200}} */}
                            <View style={{bottom:hp("0.3%")}}>
                            <Text style={{fontSize:22,fontWeight:'bold', color:"#0E4C75",}}>{pin.code}</Text>
                            <Text style={{fontSize:11,color:'#0E4C75'}}>Delivery Date:</Text>
                            {/* <Text style={{fontSize:13,fontWeight:'bold',color:'#676b6b'}}>Pending:{pin.isPending}</Text> */}
                            <View style={{flexDirection:'row'}}>
                            </View>
                            </View>
                            {/* <View style={{position:'absolute',right:30}}>
                            <Image style={{width:15,height:15}} source={require('../assets/images/singlesmall.png')}/>
                            </View> */}
                            <View style={{ backgroundColor:'#FFE9E9', borderRadius:wp("7%"),position:'absolute',width: wp("11%"),height:hp("5.1%"),marginLeft:wp("75%")}}>
                                <Text style={{textAlign:'center', color:'#EB0000',fontWeight:'bold',top:hp("1.3%")}}>{pin.isPending}</Text>
                  
                            </View>
                        </TouchableOpacity>
                        )
                            }
                            else  return null;
                            
                        }
                        
                    })
                }
                    </View>
            </ScrollView>
        </PTRView>
    )
}


const styles = StyleSheet.create({
    header:{
        height:20,
        margin:5

    },
    input: {
        height: 40,
        marginTop:3,
        marginBottom:7,
        marginLeft:7,
        marginRight:7,
        borderWidth: 1,
      },

}) 

const styles5=StyleSheet.create({
    container:{
        padding:20,
    backgroundColor:'#FFFFFF',
    borderRadius:10,
    marginTop:8,
    marginLeft:2,
    marginRight:2,
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "blue",
    shadowRadius: 4,
    elevation: 6,
    marginBottom:16,
    //   padding:20,
    //   backgroundColor:'#FFFFFF',
    //   borderRadius:16,
      
    //   flexDirection: "row",
    //   alignItems: "center",
    //   shadowColor: "#000",
    //   shadowOffset: {
    //     width: 0,
    //     height: 4,
    //   },
    //   shadowOpacity: 0.05,
    //   shadowRadius: 4,
    //   elevation: 4,
      
    //   marginBottom:16,
    }
  })
export default Allpins;