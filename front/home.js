import axios from 'axios';
import React, { useState, useEffect, useContext } from 'react';
import PTRView from 'react-native-pull-to-refresh';
import AppContext from '../../AppContext';
//import { SafeAreaView, StyleSheet, TextInput } from "react-native";
import { Card, Left, CardItem, Icon, Right, Body } from 'native-base';
import { Authheader } from '../../keys';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Button,
  Text,
  TextInput,
  View,
  Linking,
  TouchableOpacity,
  Image
} from 'react-native';

const Homeroot = ({ navigation }) => {

  const myContext = useContext(AppContext)
  const [image, set_image] = useState("")
  const [cap_name, setcapname] = useState("");
  const [com_name, setcomname] = useState("");
  const [no_of_orders, set_no_of_orders] = useState(0);
  const storeData = async () => {
    try {
      await AsyncStorage.removeItem('captain_id')
      await AsyncStorage.removeItem('captain_mobile')
      await AsyncStorage.removeItem('team_id')
      console.log("removed data")
    } catch (e) {
      console.log('came here')
      // saving error
    }
  }


  const logoutfn = async () => {
    await storeData();
    myContext.setloginvalue(false)

  }
  const get_time = () => {
    var date_format = '12'; 
    var d = new Date();
    var hour = d.getHours();  
    var minutes = d.getMinutes();  
    var result = hour;
    var ext = '';

    if (date_format == '12') {
      if (hour > 12) {
        ext = 'PM';
        hour = (hour - 12);
        result = hour;

        if (hour < 10) {
          result = "0" + hour;
        } else if (hour == 12) {
          hour = "00";
          ext = 'AM';
        }
      }
      else if (hour < 12) {
        result = ((hour < 10) ? "0" + hour : hour);
        ext = 'AM';
      } else if (hour == 12) {
        ext = 'PM';
      }
    }

    if (minutes < 10) {
      minutes = "0" + minutes;
    }

    result = result + ":" + minutes + ' ' + ext;

    return result
  }

  const fetch_cap_details = async () => {
    await axios.get(`http://65.1.88.93:5000/order/no_of_orders_of_team?team_id=${myContext.team_id}`, Authheader)
      .then(res => {
        set_no_of_orders(res.data.size)

      })
      .catch(err => {
        console.log(err)
      })
    const authdetails = {
      captain_mobile: myContext.captain_mobile
    }
    console.log("Number = " + myContext.captain_mobile)
    await axios.post(`http://65.1.88.93:5000/captain/fetchone`, authdetails, Authheader)
      .then(async res => {
        console.log(res.data.captain_dp_id)
        setcapname(res.data.captain_name);
        if (res.data.captain_dp_id) {
          let image_test = await axios.get(`http://65.1.88.93:5000/captainimage/fetch?id=${res.data.captain_dp_id}`, Authheader)
          set_image("data:image/png;base64," + image_test.data.image)

        }


      })
      .catch(err => {
        console.log("Came to catch")
        console.log(err);
      });
  }
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      fetch_cap_details();
    });
    return
  }, [navigation]);

  const _refresh = () => {
    return new Promise((resolve) => {
      fetch_cap_details()
        .then(r => resolve())

    });
  }

  return (

    <View style={{  flex: 1, height:hp("100%"),backgroundColor: "#F2F8FF", }}>
{/* borderWidth:10, borderColor:"red" */}


      {/* <View style={styles4.container}> */}

        {/* <View> */}

        <View style={{height:hp("40%"),backgroundColor: "#F2F8FF",}}>
          {/* borderColor:"green" , borderWidth:4, */}
          <TouchableOpacity style={{ top: hp("7%"), marginLeft: wp("70%") }}>
            <View>
              {
                image ? <Image
                  style={{ width: 70, height: 70, borderRadius: 1000, borderWidth: 1, borderColor: "#0f4c75" }}
                  source={{ uri: image }}
                /> :
                  <Image
                    style={{ width: 80, height: 80 }}
                    source={require("../assets/images/edit_profile.png")}
                  />
              }
            </View>
          </TouchableOpacity>
          <View style={{ bottom: hp("3%"), left:wp("3%") }}>
          <Text style={{ textAlign: 'left', fontSize: 30, fontWeight: 'bold', color: '#0f4c75' }}>Welcome Back </Text>
          <Text style={{ textAlign: 'left', fontSize: 25, fontWeight: 'bold', color: '#0f4c75' }}>{cap_name}</Text>
          </View>

          <Text style={{ left: hp("2%"),textAlign: 'left', fontSize: 18, fontWeight: 'bold', color: '#0f4c75', top: hp("5.7%") }}>Total Orders</Text>
          <View style={{ left: hp("2%")}}>
            <Text style={{ textAlign: 'left', fontSize: 40, fontWeight: "bold", color: '#0f4c75', top: hp("5.7%") }}>{no_of_orders}</Text>
            <TouchableOpacity style={{ width: wp("30%"), height: hp("4.8%"), marginLeft: wp("61%"), backgroundColor: '#0E4C75', borderRadius: 8 }} button onPress={() => { logoutfn() }}>
              <View >
                <Text style={{ textAlign: 'center', color: 'white', top: hp("1.3%") }}>View more</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ top: hp("1.85%"),left: hp("2%"), }}>
            <Text style={{ textAlign: 'left', fontSize: 15, fontWeight: "bold", color: '#0f4c75', }}>updated today at {get_time()} </Text>
          </View>
          <TouchableOpacity style={{ left: hp("23%"), }} button onPress={() => { _refresh() }}>
            <Image
              source={require("../assets/images/reload.png")}
            />
          </TouchableOpacity>


        </View>


        {/* <View
       style={{top:100}}
       > */}
        {/* <View> */}
        {/* <TouchableOpacity> */}
        {/* <View style={{marginRight: 150, marginLeft: 80, backgroundColor:'#0f4c75', borderRadius:6,paddingVertical: 40}}> */}

        {/* style={{textAlign:'center', color:'white',paddingHorizontal:10}} */}
        {/* </TouchableOpacity> */}
        {/* </View> */}
  
      <View style={{height:hp("60%"),backgroundColor: "#F2F8FF",}}>
      {/* borderColor:"blue" , borderWidth:8, */}
        <ScrollView >
        <View style={gridStyles.container}>
        {/* <View style={gridStyles.container}> */}
          <TouchableOpacity style={gridStyles.box} button onPress={() => { navigation.navigate('Pins') }}>
            <View >
              <Text style={gridStyles.text}>Delivery</Text>
            </View>

          </TouchableOpacity>
          <TouchableOpacity style={gridStyles.box} style={gridStyles.box} button onPress={() => { navigation.navigate('CustomerList') }}>
            <View >
              <Text style={gridStyles.text}>KhataBook</Text>
            </View>

          </TouchableOpacity>
          <TouchableOpacity style={gridStyles.box} style={gridStyles.box} button onPress={() => { navigation.navigate('offers') }}>
            <View >
              <Text style={gridStyles.text}>Send Offers</Text>
            </View>

          </TouchableOpacity>
          <TouchableOpacity style={gridStyles.box} button onPress={() => { navigation.navigate('Customers') }}>
            <View >
              <Text style={gridStyles.text}>Refund</Text>
            </View>

          </TouchableOpacity>
          <TouchableOpacity style={gridStyles.box} button onPress={() => { navigation.navigate('Feedback Customers') }}>
            <View >
              <Text style={gridStyles.text}>FeedBack</Text>
            </View>

          </TouchableOpacity>
          <TouchableOpacity style={gridStyles.box}>
            <View >
              <Text style={gridStyles.text}>ComingSoon</Text>
            </View>

          </TouchableOpacity>


        {/* </View> */}
        </View>
        </ScrollView>
       </View>



        {/* </View> */}
        {/* <View style={styles4.logoApp}>
         <Image  source={require('../assets/images/chotu.jpeg')} style={styles4.logo}></Image>
       </View>
       <View style={{marginTop:50}}>
       <Text style={{textAlign:'center',fontSize:30,fontWeight:'bold',color:'#0f4c75'}}>Welcome Captain</Text>
       <Text style={{textAlign:'center',fontSize:30,fontWeight:'bold',color:'#0f4c75'}}>Vamsi!</Text>
       </View> */}
        {/* </ScrollView> */}
        {/* </View> */}

      {/* </View> */}

    </View>


  );
}


export default Homeroot;

const styles4 = StyleSheet.create({
  container: {
    // marginTop: hp("-6%"),
    width: wp("100%"),
    height: hp("9%"),
    // left: wp("5%"),
    // flex: 1, 
    borderWidth: 2,
    borderColor:"orange"

  },
  button: {

    width: 70,
    height: 20,
    left: 263,
    top: 183,
    // box-shadow: -4px -4px 18px #FFFFFF, 4px 4px 18px rgba(14, 76, 117, 0.15),
    borderRadius: 6
  },
  // buttonText: {

  // },
  logoApp: {
    marginTop: 50,
    // alignSelf: "center",
    alignItems: "center",
  },
  logo: {
    height: 100,
    width: 100,
    marginBottom: 12,

  },
  inputLogin: {
    marginTop: 50,
  }

})

const gridStyles = StyleSheet.create({

  container: {
    // width: wp("100%"),
    // height : hp("30%"),
    // top: hp("12%"),
    // left: wp("5%"),
    // right: wp("5%"),
    // flexDirection: "row",
    // backgroundColor: "#F2F8FF",
    // marginLeft: wp("-3%"),
    // marginRight: wp("-12%"),
    // marginTop: hp("2%"),
    // flex: 1,
    height:hp("70%"),
    width:wp("90%"),
    flexDirection:"row", 
    justifyContent:"center",
    flexWrap:"wrap",
    top:hp("2%"),
    // borderColor: "#000000",
    // borderWidth: 5,
    // alignContent: "center",
    justifyContent: "space-evenly",
    // flexWrap: "wrap"
  },
  box: {

    backgroundColor: '#FFFFFF',
    width: wp("25%"),
    height: hp("11%"),
    borderRadius: 18,
    // padding:hp("3%"),
     // flexWrap: "wrap"
    marginTop: hp("3.5%"),
    elevation: 10,
    shadowColor: "blue",
    // textShadowColor: "#F2F8FF",
    // marginLeft:wp("4.5%")
    left:hp("2.5%")




  },
  text: {
    textAlign: 'center',
    top: hp("4%"),
  }

})
