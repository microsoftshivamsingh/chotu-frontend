import * as React from 'react';
import PTRView from 'react-native-pull-to-refresh';
import {useState,useEffect} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { ActivityIndicator, Alert } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Container, Header, Content, Form,Card, Item, Input,Left, Label,Right,Body,Title,Icon,CardItem } from 'native-base';
import {prepaid} from "../../helper";
import AppContext from "../../AppContext";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


import Share from 'react-native-share';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    PermissionsAndroid,
    Platform,
    Text,
    useColorScheme,
    TouchableOpacity,
    TextInput,
    Image,
    
    Button,
    View,
  } from 'react-native';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import {Picker} from '@react-native-community/picker'
import { color } from 'react-native-reanimated';
import { version } from 'react';
import axios from 'axios';
import { Authheader } from '../../keys';
import { forNoAnimation } from '@react-navigation/stack/lib/typescript/src/TransitionConfigs/CardStyleInterpolators';

const Ledger=({route,navigation})=>{
  const [datapresent,set_datapresent]=useState(true)
  const [orders,set_orders]=useState([]);
  const [payments,set_payments] = useState([]);
 const [transactions,set_transactions] = useState([]);
 const [htmltrans,sethtmltrans]=useState([]);
  const { customer_mob,customer_name} = route.params;
  const myContext=React.useContext(AppContext);
  const [balance,setbalance]=useState(0);
 
  const[filePath,setFilePath]=useState('');
  const[htmlstring,sethtmlstring]=useState('<h1 style="color:blue">CHOTU</h1><h2>Payment Remainder</h2>');
  let tempstring=''
  

  const compare=(a,b)=>{
    const ad = new Date(a.date)
    const bd = new Date(b.date)
    if(ad < bd)
    return 1;
    else 
    return -1;

  }
  
  const addstarthtml=async(str)=>{
    return new Promise((resolve,reject)=>{
      sethtmlstring(str);
      setTimeout(()=>{
        resolve()
      },100)
      
    })
  }

  const addnexthtml=async(str)=>{
    return new Promise((resolve,reject)=>{
      sethtmlstring(htmlstring+str);
      setTimeout(()=>{
        resolve()
      },100)
      
    })
  }

  const addledgertohtml=async ()=>{

    return  new Promise( async (resolve,reject)=>{
        await addstarthtml('<h1 style="color:blue">CHOTU</h1><br></br><h2>Payment Remainder</h2>'+'<h4>Name: '+customer_name+'</h4>'+'<h4>PhoneNo: '+customer_mob+'</h4><h4 style="color:red">Balance remaining '+fn().toString()+'</h4>');
        
        // console.log("length of me"+htmltrans.length)
          let i=0;
          let len=orders.concat(payments).length;
          // console.log("len is",len)
          orders.concat(payments).sort(compare).map(async (transac,indx)=>{
          // console.log(i);
          if(transac.type=='order'){
             await addnexthtml('<br></br><p>Debit orderid'+transac.id+'</p>'+'<p>Date: '+transac.date.substr(0,10)+'</p>'+'<p style="color:red">Amount   '+-1*transac.invoice+'</p>');
          }
          else{
            await addnexthtml('<br></br><p>Payment type '+transac.mode_of_payment+'</p><p>Date :'+transac.date.substr(3,12)+'</p><p style="color:#5cc953">Amount :+'+transac.amount+'</p')
          }
          if(i+1==len){
            resolve("done")
          }
          i=i+1;
        })
        
      
    })
    
    
    
  }


  const isPermitted = async () => {
    
      if (Platform.OS === 'android') {
        console.log("came here")
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs access to Storage data',
          },
        );
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        alert('Write permission err', err);
        return false;
      }
    } else {
      return true;
    }
   
    
  };

  
 

  const fetch_order_details = async () =>{

    
    
    sethtmlstring('<h1 style="color:blue">CHOTU</h1><br></br><h2>Payment Remainder</h2>'+'<h4>Name: '+customer_name+'</h4>'+'<h4>PhoneNo: '+customer_mob+'</h4><h4 style="color:red">Balance remaining '+balance+'</h4>')
    
    axios.get(`http://35.154.70.53:5000/order/customer_fetch?mobile=${customer_mob}&community_id=${myContext.community}`,Authheader)
    .then(res => {
      
      
      // console.log(orders.length)
      let temp = []
      res.data.forEach(a=>{
        //console.log(a)
        if(a.invoice!=null){
          temp.push(a)
        }
      })
      
      temp.forEach(or => {or["type"]="order",or["date"]=or.date_of_order})
        
      
      set_orders([...temp])

     // console.log("from order",orders.length)
    
    })
    .then(fetch_payment_details())
  
  
  }
  const fetch_payment_details = async () =>{
     axios.get(`http://35.154.70.53:5000/payment/fetch?mobile=${customer_mob}&community_id=${myContext.community}`,Authheader)
    .then(res => {
      set_payments([])
      let temp = [...res.data] 
      temp.forEach(pay => {pay["type"]="payment",pay["date"]=pay.date_of_payment})
    

      set_payments([...temp])
        
      
     // console.log("from pay",temp.length)
      
      
    }).then(()=>{
      set_datapresent(false);
    });
    

    
  }
  /* const addtotrans=async()=>{
    return new Promise(async(resolve)=>{
      const result=await sethtmltrans([...orders,...payments]);
      while(result!=0){
        resolve("done")
        break;
      }
    });
  } */

  const createPDF=async()=>{
   
      const response=await addledgertohtml();
    
      if (await isPermitted()  ){
        let options={
            html:htmlstring,
            filename:'chotutestpdf',
            directory:'Download',
        }
        console.log(options.html)

        let file=await RNHTMLtoPDF.convert(options).then(async (file2)=>{
          console.log(file2.filePath);
        
          Alert.alert(
            "Payment Share",
            "Are you sure want to share the ledger?",
            [
              
              { text: "OK", onPress: () => shareopt(file2.filePath) },
              {text:"cancel"}
            ]
          );
        
       

        }).catch(err=>{
          console.log(err)
        })
        
    }
    
    
    }

    

    const shareopt=(filepath2)=>{
      Share.open({title: "Pdf file",
        
    url: "file:///"+filepath2,
    subject: "PDF",})
    .then((res)=>{
        console.log(res);
    })
    .catch((err)=>{
        console.log(err);
    })
    }

  
   /* useEffect (async() => {
    
         await fetch_order_details();
        // await fetch_payment_details(); 
    
  
      
  
}, []) */

useEffect( () => {
  const unsubscribe = navigation.addListener('focus', () => {
       fetch_order_details();
    });
    return 
  }, [navigation]);



_refresh =  ()=> {
  return new Promise((resolve) => {
     fetch_order_details()
     .then(r => resolve())
    
  });
}

  
  
  
  const fn =() =>{
    let t =0;
    orders.forEach(a => t-=a.invoice)
    payments.forEach(a => t+=a.amount)
    
    return t;
  }

    return(
      <PTRView onRefresh={_refresh}>

        <ScrollView nestedScrollEnabled={true}>
          <View>
          { 
             fn() < 0 ? (<View style={{backgroundColor:"#FFE9E9",flexDirection:'row',height:hp("12%"),width:wp('216%'),top:hp("4%"),}}>
        
          <View style={{top:hp("3%")}}>
             <View style={{left:wp("6%")}}>
                <Text style={{fontSize:25,fontWeight:'bold',color:'#EB0000'}}>{customer_name}</Text>
                <Text style={{fontSize:15,color:'#EB0000'}}>{customer_mob}</Text>
            </View>

            <View style={{left:wp("68%"),bottom:hp("6.5%"),flexDirection:'column'}}>

              <Text style={{fontSize:hp("3.6%"),color:"#EB0000",fontWeight:"bold",marginLeft:wp("1%")}}>{"-₹"}{-1*fn()}</Text>
            
              <View style={{left:wp("11.5%"),}}>

             <TouchableOpacity style={{left:wp("13%"),bottom:hp("0.4%")}}>
              <Image 
                source={require("../assets/images/reload1.png")}
                style={{
                tintColor: "#EB0000",
                resizeMode: "contain",
                }}
                />
            
            </TouchableOpacity>

             <Text style={{fontSize:hp("1.6%"),fontWeight:'bold',color:'#EB0000',bottom:hp("2%")}}>Balance </Text>
            </View>

             </View>

          </View>
          </View>) : (<View style={{backgroundColor:"#E5FFE5",flexDirection:'row',height:hp("12%"),width:wp('216%'),top:hp("4%"),}}>
        
          <View style={{top:hp("3%")}}>
             <View style={{left:wp("6%")}}>
            <Text style={{fontSize:25,fontWeight:'bold',color:'#08C908'}}>{customer_name}</Text>
            <Text style={{fontSize:15,color:'#08C908'}}>{customer_mob}</Text>
            </View>

            <View style={{left:wp("68%"),bottom:hp("6.5%"),flexDirection:'column'}}>

            <Text style={{fontSize:hp("3.6%"),color:"#08C908",fontWeight:"bold",marginLeft:wp("1%")}}>{"+₹"}{fn()}</Text>
            
            <View style={{left:wp("11.5%"),}}>

             <TouchableOpacity style={{left:wp("13%"),bottom:hp("0.4%")}}>
              <Image 
                source={require("../assets/images/reload1.png")}
                style={{
                tintColor: "#08C908",
                resizeMode: "contain",
                }}
                />
            
            </TouchableOpacity>

             <Text style={{fontSize:hp("1.6%"),fontWeight:'bold',color:'#08C908',bottom:hp("2%")}}>Balance </Text>
            </View>
            </View>

          </View>
          </View>)
          }
           
    
          {/* reminder button */}
          {fn()<0?   <View style={{flexDirection:'row', top:hp("4%")}}>
            <TouchableOpacity 
            style={{
                left:wp("70%"),
               }}
            >
            <Image 
              source={require("../assets/images/whatsapp.png")}
               style={{
                tintColor: "#0E4C75",
                resizeMode: "contain",
                 width:wp("7%"),
               left:wp("5%"),
              }}/>

        </TouchableOpacity>
         <Text style={{position:"absolute",right:wp("4%"),top:hp("2.5%"),fontWeight:'bold',color:"#0E4C75"}}> Remind</Text>
         </View>: null }
       

       
        
       
        
        
         {
           //const transactions = orders.concat(payments);
           
         orders.concat(payments).sort(compare).map((transac,indx) => {
          if(transac.type=="order" )
          {
           
             if(transac.payment_method == "cod")
            return(
          
          <View key={indx} >
          
          
           <TouchableOpacity  style={styles5.container} button >
                    
                     <View >

                      <View style={{bottom:hp("0.3%"),}}>
                      <Text style={{fontSize:22,fontWeight:'bold', color:"#0E4C75"}}>OrderID: {transac.id} </Text>
                      <Text style={{fontSize:11,fontWeight:'bold',color:'#0E4C75'}}>Date: {transac.date.substr(0,10)} </Text>
                      </View>
                    </View>


                    <View style={{ backgroundColor:'#FFE9E9', borderRadius:25,position:'absolute',width: wp("26%"),height:hp("4.3%"),marginLeft:wp("60%")}}>
                        <Text style={{textAlign:'center', color:'#EB0000',fontWeight:'bold',top:hp("1%")}}>- ₹{transac.invoice}</Text>
                   </View>
                   
        </TouchableOpacity>
           </View>
           
          )
          else 
          return(
            <View key={indx}>
          
            <TouchableOpacity  style={styles5.container} button >
                     
                     <View style={{bottom:hp("0.3%"),}}>
                       <View style={{flexDirection:'row'}}>
                       <Text style={{fontSize:22,fontWeight:'bold', color:"#0E4C75"}}>OrderID: {transac.id} </Text>
                       </View>
                       
                       <View style={{flexDirection:'row'}}>
                       <Text style={{fontSize:11,fontWeight:'bold',color:'#0E4C75'}}>Date: {transac.date.substr(0,10)} </Text>
                       </View>
                     </View>
                     <View style={{ marginLeft: 90,backgroundColor:'#FFE9E9', borderRadius:20,}}>
                        <Text style={{textAlign:'center', color:'#EB0000',fontWeight:'bold',top:hp("1%")}}>- ₹{transac.invoice}</Text>
                   </View>
         </TouchableOpacity>
            </View>
            
          )
            }
          else{
            if(transac.mode_of_payment==prepaid)
            {
              return (
              
              
              

                <View key={indx}>
                <TouchableOpacity  style={styles5.container} button >
                                    
                <View style={{bottom:hp("0.3%"),}}>
                  <View style={{flexDirection:'row'}}>
                  {/* PaymentType:  */}
                  <Text style={{fontSize:22,fontWeight:'bold', color:"#0E4C75"}}>{transac.mode_of_payment} </Text>
                  </View>
                  
                  <View style={{flexDirection:'row'}}>
                  {/* Date:  */}
                    <Text style={{fontSize:11,fontWeight:'bold',color:'#0E4C75'}}>{transac.date.substr(3,12)} </Text>
                  </View>
                </View>
                <View style={{backgroundColor:'#E5FFE5', borderRadius:25,position:'absolute',width: wp("26%"),height:hp("4.3%"),marginLeft:wp("60%")}}>
                <Text style={{fontSize:18,fontWeight:'bold',color:'#676b6b'}}>+{transac.amount}</Text>
                </View>
                </TouchableOpacity>
                </View>
                
                
                )
              
        
          
          }
          else{
          return (
          
          
          
          <View key={indx}>

          <TouchableOpacity  style={styles5.container} button>
                         
                <View style={{bottom:hp("0.3%"),}}>
                  <View style={{flexDirection:'row'}}>
                    <Text style={{fontSize:22,fontWeight:'bold', color:"#0E4C75"}}>{transac.mode_of_payment} </Text>
                   </View>
            
                  <View style={{flexDirection:'row'}}>
                    <Text style={{fontSize:11,fontWeight:'bold',color:'#0E4C75'}}>{transac.date.substr(3,12)} </Text>
                  </View>
                   
                </View>
                <View style={{ backgroundColor:'#E5FFE5',borderRadius:25,position:'absolute',width: wp("26%"),height:hp("4.3%"),marginLeft:wp("60%") }}>
                        <Text style={{textAlign:'center', color:'#08C908',fontWeight:'bold', top:hp("1%")}}>+ ₹{transac.amount}</Text>
                 </View>
                
          
        </TouchableOpacity>
          </View>
            
            )
          
          }
        
        
          }
      })}
      <View style={{marginTop:wp("40%"),marginLeft:8,marginRight:8,}}>
      <TouchableOpacity
          activeOpacity={0.5}
          style={styles5.buttonStyle}
           onPress={() => {navigation.navigate('paymentform',{customer_mobi:customer_mob,customer_nam:customer_name})} }
          >
        
          <Text style={styles5.textStyleWhite}>
            Add Payment
          </Text>
        
        </TouchableOpacity>
      </View>
         {/* <View style={{marginBottom:20}}>
        <Button title="Add new payment" color='#0f4c75' onPress={() => {navigation.navigate('paymentform',{customer_mobi:customer_mob,customer_nam:customer_name})} }></Button>
        <Button  title="Payment remainder PDF" color='#0f4c75' disabled={datapresent} onPress={()=>createPDF()}></Button>
        </View> */}
           
           {/* <Text>{JSON.stringify(transactions,null,2)}</Text> */}
          {/*  <Text>{JSON.stringify(test,null,2)}</Text> */}
       </View>
    </ScrollView>
    </PTRView>
    );

}

export default Ledger;


const styles5=StyleSheet.create({
  container:{
    padding:40,
    backgroundColor:'#FFFFFF',
    borderRadius:10,
    top:hp("5%"),
    marginTop:hp("1.5%"),
    marginLeft:wp("4%"),
    marginRight:wp("4%"),
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "blue",
    shadowRadius: 4,
    elevation: 6,
    marginBottom:16,
    
  },

textStyleWhite: {
    padding: 15,
    color: '#FFFFFF',
    fontWeight:"bold"
  },
 
  buttonStyle: {
    marginHorizontal:wp("2.8%"),
    bottom:hp("4%"),
    alignItems: 'center',
    backgroundColor: '#0E4C75',
    
    width: wp("92%"),
    borderRadius: 8,
    
  },



}

)

{/* <Card key = {indx}>
            <CardItem>
             
               <Body>
               <Text>PaymentType:{transac.mode_of_payment}</Text>
               
               <Text>Date:{transac.date}</Text>
               </Body>
               <Right>
                 <Text style={{color:'#5cc953',fontWeight:'bold'}}>+{transac.amount}</Text>
               </Right>
               
              </CardItem>
            </Card> */}
